const development = {
  database: 'tutunskiDb',
  username: 'root',
  password: 'root',
  host: 'localhost',
  dialect: 'mysql'
};

const testing = {
  database: 'databasename',
  username: 'username',
  password: 'password',
  host: 'localhost',
  dialect: 'mysql'
};

// const production = { 
//   database: 'tutunskidb',
//   username: 'root',
//   password: 'tutunski4545',
//   host: process.env.DB_HOST || 'localhost',
//   dialect: 'mysql'
// };

const production = {
  database: 'tutunskidb',
  username: 'propisi',
  password: 'propisi1@!',
  host: process.env.DB_HOST || 'localhost',
  dialect: 'mysql'
};

module.exports = {
  development,
  testing,
  production,
};
