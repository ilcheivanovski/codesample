const superadminRoutes = require('./routes/superadminRoutes');
const adminRoutes = require('./routes/adminRoutes');
const publicRoutes = require('./routes/publicRoutes');
const userRoutes = require('./routes/userRoutes');
const userChangePrashalnikRoutes = require('./routes/userChangePrashalnikRoutes');

const config = {
  migrate: false,
  superadminRoutes,
  publicRoutes,
  adminRoutes,
  userRoutes,
  userChangePrashalnikRoutes,
  port: process.env.PORT || '3001',
};

module.exports = config;
