const database = require('../../config/database');
const User = require('../models/User');
const Role = require('../models/Role');
const UserRole = require('../models/UserRole');
const faker = require('faker');

// models
const Komitent = require('../models/Komitent');

const Prasalnik_Mart = require('../models/Prasalnik_Mart');
const Prasalnik_Maj = require('../models/Prasalnik_Maj');
const Prasalnik_Juni = require('../models/Prasalnik_Juni');
const Prasalnik_Avgust = require('../models/Prasalnik_Avgust');
const Komitent_Prasalnik_Mart = require('../models/Komitent_Prasalnik_Mart');
const Komitent_Prasalnik_Maj = require('../models/Komitent_Prasalnik_Maj');
const Komitent_Prasalnik_Juni = require('../models/Komitent_Prasalnik_Juni');
const Komitent_Prasalnik_Avgust = require('../models/Komitent_Prasalnik_Avgust');

const dbService = (environment, migrate) => {
  const authenticateDB = () => database.authenticate();

  const dropDB = () => database.drop();

  const syncDB = () => database.sync({ alter: true });//{ alter: true }

  const successfulDBStart = () => (
    console.info('connection to the database has been established successfully')
  );

  const errorDBStart = (err) => (
    console.info('unable to connect to the database:', err)
  );

  const wrongEnvironment = () => {
    console.warn(`only development, staging, test and production are valid NODE_ENV variables but ${environment} is specified`);
    return process.exit(1);
  };

  const startMigrateTrue = async () => {
    try {
      await syncDB();
      successfulDBStart();
    } catch (err) {
      errorDBStart(err);
    }
  };

  const startMigrateFalse = async () => {
    try {
      await dropDB();
      await syncDB();
      successfulDBStart();
    } catch (err) {
      errorDBStart(err);
    }
  };

  // CREATE MOCK DATA - START

  const range = len => {
    const arr = [];
    for (let i = 0; i < len; i++) {
      arr.push(i);
    }
    return arr;
  };

  const newUser = () => {
    return {
      email: faker.internet.email(),
      password: 123456,
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      deviceId: faker.random.uuid(),
      token: faker.random.uuid(),
    };
  };

  const makeUsers = (len = 20) => {
    return range(len).map(d => {
      return {
        ...newUser()
      };
    });
  }

  const newKomitent = () => {
    return {
      fullname: `${faker.name.firstName()} ${faker.name.lastName()}`,
      address: faker.address.streetAddress(),
      contractNumber: faker.random.number(),
      reonski: faker.address.city(),
      povrsina: faker.random.number(),
      dogovorenaKolicina: faker.random.number(),
    };
  };

  const makeKomitenti = (len = 5000) => {
    return range(len).map(d => {
      return {
        ...newKomitent()
      };
    });
  }

  const createMockEntities = async () => {

    // 25 tehnichari
    const users = makeUsers(25);
    users.forEach(async (user) => {

      const tehnicharRole = await Role.findOne({
        where: {
          name: 'user'
        }
      });

      const userModel = await User.create(user);

      await UserRole.create({
        userId: userModel.id,
        roleId: tehnicharRole.id,
      });
    });

    // 3 admin
    const admin = makeUsers(3);
    admin.forEach(async (user) => {

      const adminRole = await Role.findOne({
        where: {
          name: 'admin'
        }
      });

      const userModel = await User.create(user);

      await UserRole.create({
        userId: userModel.id,
        roleId: adminRole.id
      });
    });

    /// create 5000 komitents
    const komitenti = makeKomitenti();
    komitenti.forEach(async (kom) => {

      const komitent = await Komitent.create(kom);

      const Prasalnik_Mart_1 = await Prasalnik_Mart.create({});
      const Prasalnik_Maj_1 = await Prasalnik_Maj.create({});
      const Prasalnik_Juni_1 = await Prasalnik_Juni.create({});
      const Prasalnik_Avgust_1 = await Prasalnik_Avgust.create({});

      await Komitent_Prasalnik_Mart.create({ komitentId: komitent.id, prasalnikId: Prasalnik_Mart_1.id });
      await Komitent_Prasalnik_Maj.create({ komitentId: komitent.id, prasalnikId: Prasalnik_Maj_1.id });
      await Komitent_Prasalnik_Juni.create({ komitentId: komitent.id, prasalnikId: Prasalnik_Juni_1.id });
      await Komitent_Prasalnik_Avgust.create({ komitentId: komitent.id, prasalnikId: Prasalnik_Avgust_1.id });

    });
  }

  const startDev = async () => {
    try {
      await authenticateDB();

      // just sync
      // return startMigrateTrue();

      // drop and sync

      // await startMigrateFalse();

      // createMockEntities()
      // await createDefaultSuperAdmin();

    } catch (err) {
      return errorDBStart(err);
    }
  };

  const startStage = async () => {
    try {
      await authenticateDB();

      if (migrate) {
        return startMigrateTrue();
      }

      return startMigrateFalse();
    } catch (err) {
      return errorDBStart(err);
    }
  };

  const startTest = async () => {
    try {
      await authenticateDB();
      await startMigrateFalse();
    } catch (err) {
      errorDBStart(err);
    }
  };

  const startProd = async () => {
    try {
      await authenticateDB();
      //  await startMigrateFalse();
      // createMockEntities()
      // await createDefaultSuperAdmin();
    } catch (err) {
      errorDBStart(err);
    }
  };

  const createDefaultSuperAdmin = async () => {

    await User
      .findOrCreate({
        where: { email: 'ilcheivanovski@gmail.com', password: '111111' }
      })

    await Role
      .findOrCreate({
        where: { name: 'user', }
      });

    await Role
      .findOrCreate({
        where: { name: 'admin', }
      })

    await Role
      .findOrCreate({
        where: { name: 'superadmin', }
      })


    const usersRole = await Role.findOne({
      where: {
        name: 'superadmin'
      }
    });

    const user = await User.findOne({
      where: {
        email: 'ilcheivanovski@gmail.com'
      }
    });

    await UserRole.create({
      userId: user.id,
      roleId: usersRole.id
    });
  };

  const start = async () => {
    switch (environment) {
      case 'development':
        await startDev();
        break;
      case 'staging':
        await startStage();
        break;
      case 'testing':
        await startTest();
        break;
      case 'production':
        await startProd();
        break;
      default:
        await wrongEnvironment();
    }
  };

  return {
    start,
  };
};

module.exports = dbService;
