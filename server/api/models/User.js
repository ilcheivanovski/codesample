const Sequelize = require('sequelize');
const bcryptService = require('../services/bcrypt.service');

const sequelize = require('../../config/database');

// associations
const UserRole = require('../models/UserRole');
const TehnicharKomitent = require('../models/TehnicharKomitent');

const hooks = {
  beforeCreate(user) {
    user.password = bcryptService().password(user); // eslint-disable-line no-param-reassign
  },
};

const tableName = 'users';

const User = sequelize.define('User', {
  email: {
    type: Sequelize.STRING,
    unique: true,
  },
  password: {
    type: Sequelize.STRING,
  },
  firstName: {
    type: Sequelize.STRING,
  },
  lastName: {
    type: Sequelize.STRING,
  },
  deviceId: {
    type: Sequelize.STRING,
  },
  token: {
    type: Sequelize.STRING,
  },
}, { hooks, tableName });

// Associations
User.hasOne(UserRole, {
  foreignKey: 'userId'
});
User.hasOne(TehnicharKomitent, {
  foreignKey: 'tehnicharId'
});



// eslint-disable-next-line
User.prototype.toJSON = function () {
  const values = Object.assign({}, this.get());

  delete values.password;
  delete values.token;

  return values;
};

module.exports = User;
