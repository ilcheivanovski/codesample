const Sequelize = require('sequelize');
const sequelize = require('../../config/database');

const hooks = {};
const tableName = 'TehnicharKomitents';

const TehnicharKomitent = sequelize.define('TehnicharKomitent', {
    tehnicharId: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    komitentId: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
}, {
    hooks,
    tableName,
    timestamps: false,
    charset: 'utf8',
    collate: 'utf8_unicode_ci'
});

// eslint-disable-next-line
TehnicharKomitent.prototype.toJSON = function () {
    const values = Object.assign({}, this.get());

    return values;
};

module.exports = TehnicharKomitent;
