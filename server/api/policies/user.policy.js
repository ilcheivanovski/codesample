const jwt = require('jsonwebtoken');
const authService = require('../services/auth.service');

const Role = require('../models/Role');
const UserRole = require('../models/UserRole');
const User = require('../models/User');

const secret = 'secret123123';
//process.env.NODE_ENV === 'production' ? process.env.JWT_SECRET :


// usually: "Authorization: Bearer [token]" or "token: [token]"
module.exports = async (req, res, next) => {
  let tokenToVerify;

  if (req.header('Authorization')) {
    const parts = req.header('Authorization').split(' ');

    if (parts.length === 2) {
      const scheme = parts[0];
      const credentials = parts[1];

      if (/^Bearer$/.test(scheme)) {
        tokenToVerify = credentials;
      } else {
        return res.status(401).json({ msg: 'Format for Authorization: Bearer [token]' });
      }
    } else {
      return res.status(401).json({ msg: 'Format for Authorization: Bearer [token]' });
    }
  } else if (req.body.token) {
    tokenToVerify = req.body.token;
    delete req.query.token;
  } else {
    return res.status(401).json({ msg: 'No Authorization was found' });
  }

  const user = await User
    .findOne({
      where: {
        token: tokenToVerify,
      },
    });

  // new token has been signed
  if (!user) {
    return res.status(401).json({ err: 'Old token' });
  }

  const dbTokenProps = authService().getTokenProps(user.token);
  const requestTokenProps = authService().getTokenProps(tokenToVerify);

  if (!user.token === tokenToVerify && requestTokenProps.iat < dbTokenProps.iat) {
    return res.status(401).json({ err: 'Old token' });
  }

  /// user role check
  const userId = jwt.verify(tokenToVerify, secret).id;
  const requestUserRole = await UserRole.findOne({
    where: {
      userId
    },
  });

  if (!requestUserRole) { return res.status(401).json({ msg: 'Unauthorized' }); }

  const verified = [1, 2, 3].includes(requestUserRole.roleId);

  if (verified) {
    return next();
  }

  return res.status(401).json({ msg: 'Unauthorized' });
  /// user role check
};
