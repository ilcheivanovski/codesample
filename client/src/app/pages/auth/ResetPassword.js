import React, { Component } from "react";
import { Formik } from "formik";
import { connect } from "react-redux";
import { TextField } from "@material-ui/core";
import { Link, Redirect } from "react-router-dom";
import { injectIntl } from "react-intl";
import * as auth from "../../store/ducks/auth.duck";
import { requestResetPassword } from "../../crud/auth.crud";

class ResetPassword extends Component {
  state = { isRequested: false };


  render() {
    console.log({ ppppp: this.props })
    const { intl } = this.props;
    const { isRequested } = this.state;

    if (isRequested) {
      return <Redirect to="/auth" />;
    }

    return (
      <div className="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">
        <div className="kt-login__body">
          <div className="kt-login__form">
            <div className="kt-login__title">
              <h3>
                Ресетирање лозинка
              </h3>
            </div>

            <Formik
              initialValues={{ password1: "", password2: "" }}
              validate={values => {
                const errors = {};

                if (!values.password1) {
                  errors.password1 = "Внесете ја новата лозинка";
                } else if (!values.password2) {
                  errors.password2 = "Внесете ја повторно лозинката";
                } else if (values.password1 !== values.password2) {
                  errors.password2 = "Лозинките мора да се совпаѓаат";
                }

                return errors;
              }}
              onSubmit={(values, { setStatus, setSubmitting }) => {
                requestResetPassword({ ...values, token: this.props.match.params.token })
                  .then(() => {
                    this.setState({ isRequested: true });
                  })
                  .catch(() => {
                    setSubmitting(false);
                    setStatus(
                      intl.formatMessage(
                        { id: "AUTH.VALIDATION.NOT_FOUND" },
                        { name: values.email }
                      )
                    );
                  });
              }}
            >
              {({
                values,
                status,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting
              }) => (
                  <form onSubmit={handleSubmit} className="kt-form">
                    {status && (
                      <div role="alert" className="alert alert-danger">
                        <div className="alert-text">{status}</div>
                      </div>
                    )}

                    <div className="form-group">
                      <TextField
                        type="password"
                        label="Внесете ја новата лозинка"
                        margin="normal"
                        fullWidth={true}
                        name="password1"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={values.password1}
                        helperText={touched.password1 && errors.password1}
                        error={Boolean(touched.password1 && errors.password1)}
                      />
                    </div>
                    <div className="form-group">
                      <TextField
                        type="password"
                        label="Внесете ја повторно лозинката"
                        margin="normal"
                        fullWidth={true}
                        name="password2"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={values.password2}
                        helperText={touched.password2 && errors.password2}
                        error={Boolean(touched.password2 && errors.password2)}
                      />
                    </div>

                    <div className="kt-login__actions">
                      <Link to="/auth">
                        <button
                          type="button"
                          className="btn btn-secondary btn-elevate kt-login__btn-secondary"
                        >
                          Назад
                      </button>
                      </Link>

                      <button
                        type="submit"
                        className="btn btn-primary btn-elevate kt-login__btn-primary"
                        disabled={isSubmitting}
                      >
                        Поднеси
                    </button>
                    </div>
                  </form>
                )}
            </Formik>
          </div>
        </div>
      </div>
    );
  }
}

export default injectIntl(connect(null, auth.actions)(ResetPassword));
