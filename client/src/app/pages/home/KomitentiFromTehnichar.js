/* eslint-disable no-restricted-imports */
import React, { useEffect } from "react";
import { connect } from "react-redux";
import * as auth from "../../store/ducks/auth.duck";

import clsx from "clsx";
import PropTypes from "prop-types";
import {
    makeStyles,
    lighten,
} from "@material-ui/core/styles";
import {
    Paper,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Checkbox,
    Toolbar,
    Typography,
    Tooltip,
    IconButton,
    TableSortLabel,
    TablePagination,
    Modal,
    Breadcrumbs,
    CircularProgress,
} from "@material-ui/core";
import { Link } from "react-router-dom";

import DeleteIcon from "@material-ui/icons/Delete";
import AddCircleOutline from "@material-ui/icons/AddCircleOutline";

import {
    getKomitentiByTehnicharId as getKomitentiByTehnicharIdAdmin,
    getAllTehnichari as getAllTehnichariAdmin,
    removeKomitentiFromtehnichar as removeKomitentiFromtehnicharAdmin
} from "../../crud/admin.crud";
import {
    getKomitentiByTehnicharId as getKomitentiByTehnicharIdSuperAdmin,
    getAllTehnichari as getAllTehnichariSuperadmin,
    removeKomitentiFromtehnichar as removeKomitentiFromtehnicharSuperadmin
} from "../../crud/superadmin.crud";

import Moment from 'react-moment';

// Example 3
function desc3(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function stableSort3(array, cmp) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
    return order === "desc"
        ? (a, b) => desc3(a, b, orderBy)
        : (a, b) => -desc3(a, b, orderBy);
}

const headRows = [
    { id: "fullname", numeric: false, disablePadding: false, label: "Име и Презиме" },
    { id: "address", numeric: false, disablePadding: false, label: "Адреса" },
    { id: "contractNumber", numeric: false, disablePadding: false, label: "Број на договор" },
    { id: "reonski", numeric: false, disablePadding: false, label: "Реонски" },
    { id: "povrsina", numeric: false, disablePadding: false, label: "Површина" },
    { id: "dogovorenaKolicina", numeric: false, disablePadding: false, label: "Договорена количина" },
    { id: "createdAt", numeric: false, disablePadding: false, label: "Креиран на дата" },
    { id: "updatedAt", numeric: false, disablePadding: false, label: "Изменет профил на дата" },
];

function EnhancedTableHead3(props) {
    const {
        onSelectAllClick,
        order,
        orderBy,
        numSelected,
        rowCount,
        onRequestSort
    } = props;
    const createSortHandler = property => event => {
        onRequestSort(event, property);
    };

    return (
        <TableHead>
            <TableRow>
                <TableCell padding="checkbox">
                    <Checkbox
                        indeterminate={numSelected > 0 && numSelected < rowCount}
                        checked={numSelected === rowCount}
                        onChange={onSelectAllClick}
                        inputProps={{ "aria-label": "Select all desserts" }}
                    />
                </TableCell>
                {headRows.map(row => (
                    <TableCell
                        key={row.id}
                        align={row.numeric ? "right" : "left"}
                        padding={row.disablePadding ? "none" : "default"}
                        sortDirection={orderBy === row.id ? order : false}
                    >
                        <TableSortLabel
                            active={orderBy === row.id}
                            direction={order}
                            onClick={createSortHandler(row.id)}
                        >
                            {row.label}
                        </TableSortLabel>
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

EnhancedTableHead3.propTypes = {
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired
};

const useToolbarStyles3 = makeStyles(theme => ({
    root: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(1)
    },
    highlight:
        theme.palette.type === "light"
            ? {
                color: theme.palette.secondary.main,
                backgroundColor: lighten(theme.palette.secondary.light, 0.85)
            }
            : {
                color: theme.palette.text.primary,
                backgroundColor: theme.palette.secondary.dark
            },
    spacer: {
        flex: "1 1 100%"
    },
    actions: {
        color: theme.palette.text.secondary,
        display: 'flex'
    },
    title: {
        flex: "0 0 auto"
    }
}));

const EnhancedTableToolbar3 = props => {
    const classes = useToolbarStyles3();
    const { numSelected } = props;

    return (
        <Toolbar
            className={clsx(classes.root, {
                [classes.highlight]: numSelected > 0
            })}
        >
            <div className={classes.title}>
                {
                    numSelected > 0 ? (
                        <Typography color="inherit" variant="subtitle1">
                            {numSelected} селектирани
                        </Typography>
                    ) : null
                }
            </div>
            <div className={classes.spacer} />
            <div className={classes.actions}>
                {
                    numSelected > 0 ? (
                        <>
                            <Tooltip title="Избриши од база">
                                <IconButton onClick={props.handleOpenDelete} aria-label="Избриши">
                                    <DeleteIcon />
                                </IconButton>
                            </Tooltip>
                        </>
                    ) : null
                }
            </div>
        </Toolbar>
    );
};

EnhancedTableToolbar3.propTypes = {
    numSelected: PropTypes.number.isRequired
};

const useStyles3 = makeStyles(theme => ({
    root: {
        width: "100%",
        marginTop: theme.spacing(3)
    },
    paper: {
        width: "100%",
        marginBottom: theme.spacing(2)
    },
    table: {
        minWidth: 750
    },
    tableWrapper: {
        overflowX: "auto"
    }
}));

function getModalStyle() {
    const top = 50;
    const left = 50;

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}

const useStyles = makeStyles(theme => ({
    paper: {
        position: 'absolute',
        width: 400,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(4),
        outline: 'none',
    },
}));

function KomitentiFromTehnichar(props) {

    // Example 3
    const classes3 = useStyles3();
    const [order3, setOrder3] = React.useState("asc");
    const [orderBy3, setOrderBy3] = React.useState("calories");
    const [selected3, setSelected3] = React.useState([]);
    const [page3, setPage3] = React.useState(0);
    const [dense3, setDense3] = React.useState(false);
    const [rowsPerPage3, setRowsPerPage3] = React.useState(100);
    const [rows, setRows] = React.useState([]);
    const param = Number(props.match.params.userId);
    const [loading, setLoading] = React.useState(true);
    const [loadingDelete, setLoadingDelete] = React.useState(false);

    function handleRequestSort3(event, property) {
        const isDesc = orderBy3 === property && order3 === "desc";
        setOrder3(isDesc ? "asc" : "desc");
        setOrderBy3(property);
    }

    function handleSelectAllClick3(event) {
        if (event.target.checked) {
            const newSelecteds = rows.map(n => n.id);
            setSelected3(newSelecteds);
            return;
        }
        setSelected3([]);
    }

    function handleClickCheckbox(event, name) {
        const selectedIndex = selected3.indexOf(name);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected3, name);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected3.slice(1));
        } else if (selectedIndex === selected3.length - 1) {
            newSelected = newSelected.concat(selected3.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected3.slice(0, selectedIndex),
                selected3.slice(selectedIndex + 1)
            );
        }

        setSelected3(newSelected);
    }

    function handleChangeProfile(event, userId) {
        props.history.push(`/komitent/${userId}`);
    }

    function handleChangePage3(event, newPage) {
        setPage3(newPage);
    }

    function handleChangeRowsPerPage3(event) {
        setRowsPerPage3(+event.target.value);
    }


    const komitentiAsync = async () => {
        const response = props.user.UserRole.roleId === 3 ? await getKomitentiByTehnicharIdSuperAdmin({ id: param }) : await getKomitentiByTehnicharIdAdmin({ id: param });
        setRows(response && response.data.komitenti);
        setLoading(false);
    }

    const isSelected3 = name => selected3.indexOf(name) !== -1;

    const emptyRows3 =
        rowsPerPage3 - Math.min(rowsPerPage3, rows.length - page3 * rowsPerPage3);


    useEffect(() => {
        komitentiAsync(props);
    }, []);

    const enumToAdminName = (roleId) => {
        switch (roleId) {
            case 1: {
                return 'Техничар';
            }
            case 2: {
                return 'Админ';
            }
            case 3: {
                return 'Супеер админ';
            }
            default:
                return null;
        }
    };
    const [openDelete, setOpenDelete] = React.useState(false);
    const [openAdd, setOpenAdd] = React.useState(false);
    const [tehnichari, setTehnichari] = React.useState([]);
    // getModalStyle is not a pure function, we roll the style only on the first render
    const [modalStyle] = React.useState(getModalStyle);

    const handleOpenDelete = () => {
        setOpenDelete(true);
    };

    const handleCloseDelete = () => {
        setSelected3([]);
        setOpenDelete(false);
    };
    const handleOpenAdd = async () => {
        const tehnichari1 = props.user.UserRole.roleId === 3 ? await getAllTehnichariSuperadmin() : await getAllTehnichariAdmin();

        setTehnichari(tehnichari1.data.tehnichari);
        setOpenAdd(true);
    };

    const classes = useStyles();

    const handleDeleteUsers = async () => {

        setLoadingDelete(true);

        props.user.UserRole.roleId === 3
            ? await removeKomitentiFromtehnicharSuperadmin({ komitentiIds: selected3, tehnichariIds: [param] })
            : await removeKomitentiFromtehnicharAdmin({ komitentiIds: selected3, tehnichariIds: [param] });

        const response = props.user.UserRole.roleId === 3
            ? await getKomitentiByTehnicharIdSuperAdmin({ id: param })
            : await getKomitentiByTehnicharIdAdmin({ id: param });

        setRows(response && response.data.komitenti);

        setSelected3([]);
        setLoadingDelete(false);
        setOpenDelete(false);
    }

    return (
        <>
            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={openDelete}
                onClose={handleCloseDelete}
            >
                <div style={modalStyle} className={classes.paper}>
                    <div variant="h6" id="modal-title" className="delete-user-m-header">
                        Отстрани комитенти од техничар
                    </div>
                    <div variant="subtitle1" id="simple-modal-description" className="delete-user-m-body">
                        Дали сакате да ги отстраните селектираните комитенти?
                    </div>
                    <div className="kt-login__actions delete-user-m-action-buttons">
                        <button
                            type="button"
                            className="btn btn-secondary btn-elevate kt-login__btn-secondary"
                            onClick={handleCloseDelete}
                        >
                            Назад
                        </button>


                        <button
                            type="submit"
                            className={
                                `btn btn-primary btn-elevate kt-login__btn-primary ${clsx(
                                    {
                                        "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": loadingDelete
                                    }
                                )}`
                            }
                            disabled={loadingDelete}
                            onClick={async () => handleDeleteUsers()}
                        >
                            Бриши
                        </button>
                    </div>
                </div>
            </Modal>
            <div className="kt-portlet">
                <div className="kt-portlet__head">
                    <Breadcrumbs aria-label="Breadcrumb" style={{ marginTop: '17px', fontSize: '18px' }}>
                        <Link color="inherit" to="/users" >
                            Техничари-Админи
                        </Link>
                        <Link color="inherit" to={`/editUser/${Number(props.match.params.userId)}`} >
                            Едитирање техничар
                        </Link>
                        <div color="textPrimary">
                            Доделени комитенти
                        </div>
                    </Breadcrumbs>
                </div>

                <div className='kt-portlet__body'>

                    <EnhancedTableToolbar3
                        numSelected={selected3.length}
                        selected3={selected3}
                        handleOpenDelete={handleOpenDelete}
                        handleOpenAdd={handleOpenAdd}
                    />
                    <div className={classes3.tableWrapper}>
                        {loading ? <div className="circular-progress "><CircularProgress /></div> : null}
                        <Table
                            className={classes3.table}
                            aria-labelledby="tableTitle"
                            size={dense3 ? "small" : "medium"}
                        >
                            <EnhancedTableHead3
                                match={props.match}
                                numSelected={selected3.length}
                                order={order3}
                                orderBy={orderBy3}
                                onSelectAllClick={handleSelectAllClick3}
                                onRequestSort={handleRequestSort3}
                                rowCount={rows.length}
                            />
                            <TableBody>
                                {
                                    stableSort3(rows, getSorting(order3, orderBy3))
                                        .slice(
                                            page3 * rowsPerPage3,
                                            page3 * rowsPerPage3 + rowsPerPage3
                                        )
                                        .map((row, index) => {
                                            const isItemSelected = isSelected3(row.id);
                                            const labelId = `enhanced-table-checkbox-${index}`;

                                            return (
                                                <TableRow
                                                    style={{ cursor: 'pointer' }}
                                                    hover
                                                    role="checkbox"
                                                    aria-checked={isItemSelected}
                                                    tabIndex={-1}
                                                    key={index}
                                                    selected={isItemSelected}
                                                >
                                                    <TableCell padding="checkbox">
                                                        <Checkbox
                                                            onClick={event =>
                                                                handleClickCheckbox(event, row.id)
                                                            }
                                                            checked={isItemSelected}
                                                            inputProps={{
                                                                "aria-labelledby": labelId
                                                            }}
                                                        />
                                                    </TableCell>
                                                    <TableCell
                                                        onClick={event =>
                                                            handleChangeProfile(event, row.id)
                                                        }
                                                        lign="left"
                                                    >
                                                        {row.fullname}
                                                    </TableCell>
                                                    <TableCell
                                                        onClick={event =>
                                                            handleChangeProfile(event, row.id)
                                                        }
                                                        align="left"
                                                    >
                                                        {row.address}
                                                    </TableCell>
                                                    <TableCell
                                                        onClick={event =>
                                                            handleChangeProfile(event, row.id)
                                                        }
                                                        align="left"
                                                    >
                                                        {row.contractNumber}
                                                    </TableCell>
                                                    <TableCell
                                                        onClick={event =>
                                                            handleChangeProfile(event, row.id)
                                                        }
                                                        align="left"
                                                    >
                                                        {row.reonski}
                                                    </TableCell>
                                                    <TableCell
                                                        onClick={event =>
                                                            handleChangeProfile(event, row.id)
                                                        }
                                                        align="left"
                                                    >
                                                        {row.povrsina}
                                                    </TableCell>
                                                    <TableCell
                                                        onClick={event =>
                                                            handleChangeProfile(event, row.id)
                                                        }
                                                        align="left"
                                                    >
                                                        {row.dogovorenaKolicina}
                                                    </TableCell>
                                                    <TableCell
                                                        onClick={event =>
                                                            handleChangeProfile(event, row.id)
                                                        }
                                                        align="left"
                                                    >
                                                        <Moment format="DD - MMMM - YYYY">
                                                            {row.createdAt}
                                                        </Moment>
                                                    </TableCell>
                                                    <TableCell
                                                        onClick={event =>
                                                            handleChangeProfile(event, row.id)
                                                        }
                                                        align="left"
                                                    >
                                                        <Moment format="DD - MMMM - YYYY">
                                                            {row.updatedAt}
                                                        </Moment>
                                                    </TableCell>
                                                </TableRow>
                                            );
                                        })}
                                {emptyRows3 > 0 && (
                                    <TableRow style={{ height: 49 * emptyRows3 }}>
                                        <TableCell colSpan={6} />
                                    </TableRow>
                                )}
                            </TableBody>
                        </Table>
                    </div>
                    <TablePagination
                        rowsPerPageOptions={[10, 25, 50, 100]}
                        component="div"
                        count={rows.length}
                        rowsPerPage={rowsPerPage3}
                        page={page3}
                        backIconButtonProps={{
                            "aria-label": "Previous Page"
                        }}
                        nextIconButtonProps={{
                            "aria-label": "Next Page"
                        }}
                        onChangePage={handleChangePage3}
                        onChangeRowsPerPage={handleChangeRowsPerPage3}
                    />
                </div>
            </div>
        </>
    );
}


const mapStateToProps = ({ auth: { user } }) => ({
    user
});

export default connect(mapStateToProps, auth.actions)(KomitentiFromTehnichar);
