import React, { useState, useEffect } from "react";
import { Formik } from "formik";
import { connect } from "react-redux";
import { TextField } from "@material-ui/core";
import clsx from "clsx";

import { Link } from "react-router-dom";
import * as auth from "../../store/ducks/auth.duck";

import {
    createKomitent as createKomitentAdmin,
    getAllTehnichari as getAllTehnichariAdmin,
} from "../../crud/admin.crud";
import {
    createKomitent as createKomitentSuperadmin,
    getAllTehnichari as getAllTehnichariSuperadmin,
} from "../../crud/superadmin.crud";

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    dense: {
        marginTop: 16,
    },
    menu: {
        width: 200,
    },
}));

function CreateKomitent(props) {

    const classes = useStyles();
    const [loading, setLoading] = useState(false);
    const [loadingButtonStyle, setLoadingButtonStyle] = useState({
        paddingRight: "2.5rem"
    });
    const [tehnichari, setTehnichari] = useState([{ value: '', label: 'Одбери' }]);

    const enableLoading = () => {
        setLoading(true);
        setLoadingButtonStyle({ paddingRight: "3.5rem" });
    };

    const disableLoading = () => {
        setLoading(false);
        setLoadingButtonStyle({ paddingRight: "2.5rem" });
    };

    const getTehnichari = async () => {
        setLoading(true);

        const alltehnichari = props.user.UserRole.roleId === 3
            ? await getAllTehnichariSuperadmin()
            : await getAllTehnichariAdmin();

        const tehnichari = alltehnichari.data.tehnichari.reduce((acc, el) => {
            const tehnichar = {
                value: el.id,
                label: `${el.firstName} ${el.lastName}`
            }
            acc.push(tehnichar)
            return acc;
        }, [{ value: '', label: 'Одбери' }]);

        setTehnichari(tehnichari);
        setLoading(false);
    }

    useEffect(() => {
        getTehnichari();
    }, []);

    return (
        <div className="kt-portlet">
            <div className="kt-portlet__head">
                <h3 style={{ marginTop: '17px' }}>
                    Креирање на комитент
                 </h3>
            </div>

            <div className='kt-portlet__body'>

                <Formik
                    style={{ backgroundColor: 'white' }}
                    initialValues={{
                        fullname: '',
                        address: '',
                        contractNumber: '',
                        reonski: '',
                        povrsina: '',
                        dogovorenaKolicina: ''
                    }}
                    validate={values => {
                        const errors = {};

                        if (!values.fullname) {
                            errors.fullname = 'Полето е задолжително';
                        } else if (!values.address) {
                            errors.address = 'Полето е задолжително';
                        } else if (!values.contractNumber) {
                            errors.contractNumber = 'Полето е задолжително';
                        } else if (!values.reonski) {
                            errors.reonski = 'Полето е задолжително';
                        } else if (!values.povrsina) {
                            errors.povrsina = 'Полето е задолжително';
                        } else if (!values.dogovorenaKolicina) {
                            errors.dogovorenaKolicina = 'Полето е задолжително';
                        }

                        return errors;
                    }}
                    onSubmit={
                        (values, { setStatus, setSubmitting }) => {
                            enableLoading();
                            setSubmitting(true);

                            const promise = props.user.UserRole.roleId === 3
                                ? createKomitentSuperadmin(values)
                                : createKomitentAdmin(values);

                            promise
                                .then((result) => {

                                    disableLoading();
                                    setSubmitting(false);
                                    setStatus('');

                                    props.history.push('/komitenti');
                                })
                                .catch((error) => {
                                    disableLoading();
                                    setSubmitting(false);
                                    setStatus(error && error.response && error.response.data.msg || 'Грешка!');
                                });
                        }
                    }
                >
                    {({
                        values,
                        status,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting,
                    }) => (
                            <form onSubmit={handleSubmit} className="kt-form">
                                {status && (
                                    <div role="alert" className="alert alert-danger">
                                        <div className="alert-text">{status}</div>
                                    </div>
                                )}

                                <div className="form-group">
                                    <TextField
                                        type="text"
                                        label="Име и презиме"
                                        margin="normal"
                                        fullWidth={true}
                                        name="fullname"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.fullname}
                                        helperText={touched.fullname && errors.fullname}
                                        error={Boolean(touched.fullname && errors.fullname)}
                                    />
                                </div>

                                <div className="form-group">
                                    <TextField
                                        type="text"
                                        label="Адреса"
                                        margin="normal"
                                        fullWidth={true}
                                        name="address"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.address}
                                        helperText={touched.address && errors.address}
                                        error={Boolean(touched.address && errors.address)}
                                    />
                                </div>

                                <div className="form-group">
                                    <TextField
                                        type="text"
                                        label="Број на договор"
                                        margin="normal"
                                        fullWidth={true}
                                        name="contractNumber"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.contractNumber}
                                        helperText={touched.contractNumber && errors.contractNumber}
                                        error={Boolean(touched.contractNumber && errors.contractNumber)}
                                    />
                                </div>

                                <div className="form-group">
                                    <TextField
                                        InputLabelProps={{ shrink: true }}
                                        name="reonski"
                                        id="filled-select-currency-native"
                                        select
                                        label="Реонски"
                                        SelectProps={{ native: true }}
                                        margin="normal"
                                        className="kt-width-full"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.reonski}
                                        helperText={touched.reonski && errors.reonski}
                                        error={Boolean(touched.reonski && errors.reonski)}
                                    >
                                        {
                                            tehnichari.map(option => (
                                                <option key={option.value} value={option.value}>
                                                    {option.label}
                                                </option>
                                            ))
                                        }
                                    </TextField>
                                </div>

                                <div className="form-group">
                                    <TextField
                                        type="text"
                                        margin="normal"
                                        label="Површина"
                                        className="kt-width-full"
                                        name="povrsina"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.povrsina}
                                        helperText={touched.povrsina && errors.povrsina}
                                        error={Boolean(
                                            touched.povrsina && errors.povrsina
                                        )}
                                    />
                                </div>

                                <div className="form-group">
                                    <TextField
                                        type="text"
                                        margin="normal"
                                        label="Договорена колоичина"
                                        className="kt-width-full"
                                        name="dogovorenaKolicina"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.dogovorenaKolicina}
                                        helperText={touched.dogovorenaKolicina && errors.dogovorenaKolicina}
                                        error={Boolean(
                                            touched.dogovorenaKolicina && errors.dogovorenaKolicina
                                        )}
                                    />
                                </div>

                                <div className="kt-login__actions">
                                    <Link to="/dashboard">
                                        <button
                                            type="button"
                                            className="btn btn-secondary btn-elevate kt-login__btn-secondary"
                                        >
                                            Назад
                                            </button>
                                    </Link>

                                    <button
                                        type="submit"
                                        style={{ marginLeft: '10px' }}
                                        className={
                                            `btn btn-primary btn-elevate kt-login__btn-primary ${clsx(
                                                {
                                                    "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": loading
                                                }
                                            )}`
                                        }
                                        disabled={isSubmitting}
                                    >
                                        Поднеси
                                        </button>
                                </div>
                            </form>
                        )}
                </Formik>
            </div>

        </div>
    );
}

const mapStateToProps = ({ auth: { user } }) => ({
    user
});

export default connect(mapStateToProps, auth.actions)(CreateKomitent);