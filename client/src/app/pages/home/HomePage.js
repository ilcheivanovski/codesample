import React, { Suspense, lazy } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import Builder from "./Builder";
import DocsPage from "./docs/DocsPage";
import { LayoutSplashScreen } from "../../../_metronic";
///app pages
import Dashboard from "./Dashboard";
import Profile from "./Profile";

import Users from "./Users";
import CreateUser from "./CreateUser";
import EditUser from "./EditUser";

import Komitenti from "./Komitenti";
import CreateKomitent from "./CreateKomitent";
import Komitent from "./Komitent";

import EditMartPrasalnik from "./prashalnici/EditMartPrasalnik";
import EditMajPrasalnik from "./prashalnici/EditMajPrasalnik";
import EditJuniPrasalnik from "./prashalnici/EditJuniPrasalnik";
import EditAvgustPrasalnik from "./prashalnici/EditAvgustPrasalnik";
import KomitentiFromTehnichar from "./KomitentiFromTehnichar";

const GoogleMaterialPage = lazy(() =>
  import("./google-material/GoogleMaterialPage")
);
const ReactBootstrapPage = lazy(() =>
  import("./react-bootstrap/ReactBootstrapPage")
);

export default function HomePage() {
  // useEffect(() => {
  //   console.log('Home page');
  // }, []) // [] - is required if you need only one call
  // https://reactjs.org/docs/hooks-reference.html#useeffect

  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        {
          /* Redirect from root URL to /dashboard. */
          <Redirect exact from="/" to="/dashboard" />
        }
        <Route path="/dashboard" component={Dashboard} />
        <Route path="/profile" component={Profile} />

        <Route path="/users" component={Users} />
        <Route path="/createUser" component={CreateUser} />
        <Route exact path="/editUser/:userId" component={EditUser} />

        <Route path="/komitenti" component={Komitenti} />
        <Route path="/createKomitent" component={CreateKomitent} />
        <Route exact path="/komitent/:komitentId" component={Komitent} />

        <Route path="/komitent/:komitentId/martPrasalnik/:prasalnikId" component={EditMartPrasalnik} />
        <Route path="/komitent/:komitentId/majPrasalnik/:prasalnikId" component={EditMajPrasalnik} />
        <Route path="/komitent/:komitentId/juniPrasalnik/:prasalnikId" component={EditJuniPrasalnik} />
        <Route path="/komitent/:komitentId/avgustPrasalnik/:prasalnikId" component={EditAvgustPrasalnik} />

        <Route path="/editUser/:userId/komitenti" component={KomitentiFromTehnichar} />

        <Route path="/builder" component={Builder} />
        <Route path="/google-material" component={GoogleMaterialPage} />
        <Route path="/react-bootstrap" component={ReactBootstrapPage} />
        <Route path="/docs" component={DocsPage} />
        <Redirect to="/error/error-v1" />
      </Switch>
    </Suspense>
  );
}
