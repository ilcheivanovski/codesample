/* eslint-disable no-restricted-imports */
import React, { useEffect } from "react";
import clsx from "clsx";
import PropTypes from "prop-types";
import {
    makeStyles,
    lighten,
} from "@material-ui/core/styles";
import {
    Paper,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Checkbox,
    Toolbar,
    Typography,
    IconButton,
    TableSortLabel,
    TablePagination,
    Modal,
    Button,
    CircularProgress,
} from "@material-ui/core";

import { addKomitentiToTehnichari as addKomitentiToTehnichariAdmin } from "../../../crud/admin.crud";
import { addKomitentiToTehnichari as addKomitentiToTehnichariSuperadmin } from "../../../crud/superadmin.crud";

import Moment from 'react-moment';

// Example 3
function desc3(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function stableSort3(array, cmp) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
    return order === "desc"
        ? (a, b) => desc3(a, b, orderBy)
        : (a, b) => -desc3(a, b, orderBy);
}

const headRows = [
    { id: "email", numeric: false, disablePadding: false, label: "Email" },
    { id: "firstName", numeric: false, disablePadding: false, label: "Име" },
    { id: "lastName", numeric: false, disablePadding: false, label: "Презиме" },
    { id: "createdAt", numeric: false, disablePadding: false, label: "Креиран на дата" },
    { id: "updatedAt", numeric: false, disablePadding: false, label: "Изменет профил на дата" },
];

function EnhancedTableHead3(props) {
    const {
        onSelectAllClick,
        order,
        orderBy,
        numSelected,
        rowCount,
        onRequestSort
    } = props;
    const createSortHandler = property => event => {
        onRequestSort(event, property);
    };

    return (
        <TableHead>
            <TableRow>
                <TableCell padding="checkbox">
                    <Checkbox
                        indeterminate={numSelected > 0 && numSelected < rowCount}
                        checked={numSelected === rowCount}
                        onChange={onSelectAllClick}
                        inputProps={{ "aria-label": "Select all desserts" }}
                    />
                </TableCell>
                {
                    headRows.map(row => (
                        <TableCell
                            key={row.id}
                            align={row.numeric ? "right" : "left"}
                            padding={row.disablePadding ? "none" : "default"}
                            sortDirection={orderBy === row.id ? order : false}
                        >
                            <TableSortLabel
                                active={orderBy === row.id}
                                direction={order}
                                onClick={createSortHandler(row.id)}
                            >
                                {row.label}
                            </TableSortLabel>
                        </TableCell>
                    ))
                }
            </TableRow>
        </TableHead>
    );
}

EnhancedTableHead3.propTypes = {
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired
};

const useToolbarStyles3 = makeStyles(theme => ({
    root: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(1)
    },
    highlight:
        theme.palette.type === "light"
            ? {
                color: theme.palette.secondary.main,
                backgroundColor: lighten(theme.palette.secondary.light, 0.85)
            }
            : {
                color: theme.palette.text.primary,
                backgroundColor: theme.palette.secondary.dark
            },
    spacer: {
        flex: "1 1 100%"
    },
    actions: {
        color: theme.palette.text.secondary
    },
    title: {
        flex: "0 0 auto"
    }
}));

const EnhancedTableToolbar3 = props => {
    const classes = useToolbarStyles3();
    const { numSelected } = props;

    return (
        <Toolbar
            className={clsx(classes.root, {
                [classes.highlight]: numSelected > 0
            })}
        >
            <div className={classes.title}>
                {
                    numSelected > 0 ? (
                        <Typography color="inherit" variant="subtitle1">
                            {numSelected} селектиран
                        </Typography>
                    ) : null
                }
            </div>
            <div className={classes.spacer} />
        </Toolbar>
    );
};

EnhancedTableToolbar3.propTypes = {
    numSelected: PropTypes.number.isRequired
};

const useStyles3 = makeStyles(theme => ({
    root: {
        width: "100%",
        marginTop: theme.spacing(3)
    },
    paper: {
        width: "100%",
        marginBottom: theme.spacing(2)
    },
    table: {
        minWidth: 750
    },
    tableWrapper: {
        overflowX: "auto"
    }
}));

function getModalStyle() {
    const top = 50;
    const left = 50;

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
        width: '90%',
        height: '90%',
        overflow: 'auto'
    };
}

const useStyles = makeStyles(theme => ({
    paper: {
        position: 'absolute',
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(4),
        outline: 'none',
    },
}));

function AddKomitentiModal(props) {

    // Example 3
    const classes3 = useStyles3();
    const [order3, setOrder3] = React.useState("asc");
    const [orderBy3, setOrderBy3] = React.useState("calories");
    const [selected3, setSelected3] = React.useState([]);
    const [page3, setPage3] = React.useState(0);
    const [dense3, setDense3] = React.useState(false);
    const [rowsPerPage3, setRowsPerPage3] = React.useState(100);
    const [rows, setRows] = React.useState([]);
    const [loading, setLoading] = React.useState(false);

    React.useEffect(() => {
        setRows(props.tehnichari);
    }, [props.tehnichari])


    function handleRequestSort3(event, property) {
        const isDesc = orderBy3 === property && order3 === "desc";
        setOrder3(isDesc ? "asc" : "desc");
        setOrderBy3(property);
    }

    function handleClickCheckbox(event, name) {

        let newSelected = [];
        newSelected.push(name)

        setSelected3(newSelected);
    }

    function handleChangeProfile(event, userId) {
        props.history.push(`/editUser/${userId}`);
    }

    function handleChangePage3(event, newPage) {
        setPage3(newPage);
    }

    function handleChangeRowsPerPage3(event) {
        setRowsPerPage3(+event.target.value);
    }

    const isSelected3 = name => selected3.indexOf(name) !== -1;

    // getModalStyle is not a pure function, we roll the style only on the first render
    const [modalStyle] = React.useState(getModalStyle);

    const classes = useStyles();

    const handleAddUsers = async () => {

        const tehnichariIds = selected3;
        const komitentiIds = props.selectedKomitentiIds;

        setLoading(true);

        props.roleId === 3
            ? await addKomitentiToTehnichariSuperadmin({ tehnichariIds, komitentiIds })
            : await addKomitentiToTehnichariAdmin({ tehnichariIds, komitentiIds });

        setSelected3([]);

        await props.handleCloseAdd();

        setLoading(false);

    }

    return (
        <>
            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={props.openAdd}
                onClose={props.handleCloseAdd}
            >
                <div style={modalStyle} className={classes.paper}>
                    <div variant="h6" id="modal-title" className="delete-user-m-header">
                        Техничари
                    </div>
                    <div variant="subtitle1" id="simple-modal-description" >

                        <EnhancedTableToolbar3
                            numSelected={selected3.length}
                            selected3={selected3}
                            handleOpen={props.handleOpenAdd}
                        />

                        <div className={classes3.tableWrapper}>
                            <Table
                                className={classes3.table}
                                aria-labelledby="tableTitle"
                                size={dense3 ? "small" : "medium"}
                            >
                                <EnhancedTableHead3
                                    numSelected={selected3.length}
                                    order={order3}
                                    orderBy={orderBy3}
                                    onRequestSort={handleRequestSort3}
                                    rowCount={rows.length}
                                />
                                <TableBody>
                                    {/* {loading ? <div className="circular-progress "><CircularProgress /></div> : null} */}
                                    {
                                        stableSort3(rows, getSorting(order3, orderBy3))
                                            .slice(
                                                page3 * rowsPerPage3,
                                                page3 * rowsPerPage3 + rowsPerPage3
                                            )
                                            .map((row, index) => {
                                                const isItemSelected = isSelected3(row.id);
                                                const labelId = `enhanced-table-checkbox-${index}`;

                                                return (
                                                    <TableRow
                                                        hover
                                                        style={{ cursor: 'pointer', height: '40px' }}
                                                        role="checkbox"
                                                        aria-checked={isItemSelected}
                                                        tabIndex={-1}
                                                        key={index}
                                                        selected={isItemSelected}
                                                    >
                                                        <TableCell padding="checkbox">
                                                            <Checkbox
                                                                onClick={event =>
                                                                    handleClickCheckbox(event, row.id)
                                                                }
                                                                checked={isItemSelected}
                                                                inputProps={{
                                                                    "aria-labelledby": labelId
                                                                }}
                                                            />
                                                        </TableCell>
                                                        <TableCell
                                                            lign="left"
                                                        >
                                                            {row.email}
                                                        </TableCell>
                                                        <TableCell
                                                            align="left"
                                                        >
                                                            {row.firstName}
                                                        </TableCell>
                                                        <TableCell
                                                            align="left"
                                                        >
                                                            {row.lastName}
                                                        </TableCell>
                                                        <TableCell
                                                            align="left"
                                                        >
                                                            <Moment format="DD - MMMM - YYYY">
                                                                {row.createdAt}
                                                            </Moment>
                                                        </TableCell>
                                                        <TableCell
                                                            align="left"
                                                        >
                                                            <Moment format="DD - MMMM - YYYY">
                                                                {row.updatedAt}
                                                            </Moment>
                                                        </TableCell>
                                                    </TableRow>
                                                );
                                            })
                                    }
                                </TableBody>
                            </Table>
                        </div>
                        <TablePagination
                            rowsPerPageOptions={[10, 25, 50, 100]}
                            component="div"
                            count={rows.length}
                            rowsPerPage={rowsPerPage3}
                            page={page3}
                            backIconButtonProps={{
                                "aria-label": "Previous Page"
                            }}
                            nextIconButtonProps={{
                                "aria-label": "Next Page"
                            }}
                            onChangePage={handleChangePage3}
                            onChangeRowsPerPage={handleChangeRowsPerPage3}
                        />
                    </div>
                    <div className="kt-login__actions delete-user-m-action-buttons">
                        <button
                            type="button"
                            className="btn btn-secondary btn-elevate kt-login__btn-secondary"
                            onClick={props.handleCloseAdd}
                        >
                            Назад
                        </button>

                        <button
                            type="submit"
                            className={
                                `btn btn-primary btn-elevate kt-login__btn-primary ${clsx(
                                    {
                                        "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": loading
                                    }
                                )}`
                            }
                            disabled={loading}
                            onClick={async () => handleAddUsers()}
                        >
                            Додели
                        </button>
                    </div>
                </div>
            </Modal>
        </>
    );
}

export default AddKomitentiModal;