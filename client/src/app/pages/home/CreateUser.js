import React, { useState, useEffect } from "react";
import { Formik } from "formik";
import { connect } from "react-redux";
import { TextField } from "@material-ui/core";
import clsx from "clsx";

import { Link, Redirect } from "react-router-dom";
import * as auth from "../../store/ducks/auth.duck";
import { createProfile } from "../../crud/superadmin.crud";
import { createTehnichar } from "../../crud/admin.crud";
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    dense: {
        marginTop: 16,
    },
    menu: {
        width: 200,
    },
}));

const userRoles = [
    {
        value: 'user',
        label: 'Техничар',
    },
    {
        value: 'admin',
        label: 'Админ',
    },
];

function CreateUser(props) {

    const classes = useStyles();
    const [loading, setLoading] = useState(false);
    const [loadingButtonStyle, setLoadingButtonStyle] = useState({
        paddingRight: "2.5rem"
    });

    const enableLoading = () => {
        setLoading(true);
        setLoadingButtonStyle({ paddingRight: "3.5rem" });
    };

    const disableLoading = () => {
        setLoading(false);
        setLoadingButtonStyle({ paddingRight: "2.5rem" });
    };

    return (
        <div className="kt-portlet">
            <div className="kt-portlet__head">
                <h3 style={{ marginTop: '17px' }}>
                    Креирање на Техничар {props.user.UserRole.roleId === 3 ? '/Админ' : null}
                </h3>
            </div>

            <div className='kt-portlet__body'>

                <Formik
                    style={{ backgroundColor: 'white' }}
                    initialValues={{
                        role: 'user',
                        email: '',
                        firstName: '',
                        lastName: '',
                        password1: 123456,
                        password2: 123456,
                    }}
                    validate={values => {
                        const errors = {};

                        if (!values.email) {
                            // https://github.com/formatjs/react-intl/blob/master/docs/API.md#injection-api
                            errors.email = 'Полето е задолжително';
                        } else if (
                            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                        ) {
                            errors.email = 'Полето не е валидно';
                        } else if (!values.firstName) {
                            errors.firstName = 'Полето е задолжително';
                        } else if (!values.role) {
                            errors.role = 'Полето е задолжително';
                        } else if (!values.lastName) {
                            errors.lastName = 'Полето е задолжително';
                        } else if (!values.password1) {
                            errors.password1 = 'Полето е задолжително';
                        } else if (!values.password2) {
                            errors.password2 = 'Полето е задолжително';
                        } else if (values.password1 !== values.password2) {
                            errors.password2 = "Лозинките мора да се совпаѓаат";
                        }

                        return errors;
                    }}
                    onSubmit={
                        (values, { setStatus, setSubmitting }) => {

                            enableLoading();
                            setSubmitting(true);

                            const promise = props.user.UserRole.roleId === 3
                                ? createProfile(values)
                                : createTehnichar(values);

                            promise
                                .then((result) => {

                                    disableLoading();
                                    setSubmitting(false);
                                    setStatus('');

                                    props.history.push('/users');
                                })
                                .catch((error) => {
                                    disableLoading();
                                    setSubmitting(false);
                                    setStatus(error && error.response && error.response.data.msg || 'Грешка!');
                                })
                        }
                    }
                >
                    {({
                        values,
                        status,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting,
                    }) => (
                            <form onSubmit={handleSubmit} className="kt-form">
                                {status && (
                                    <div role="alert" className="alert alert-danger">
                                        <div className="alert-text">{status}</div>
                                    </div>
                                )}

                                <div className="form-group">
                                    <TextField
                                        type="email"
                                        label="Email"
                                        margin="normal"
                                        fullWidth={true}
                                        name="email"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.email}
                                        helperText={touched.email && errors.email}
                                        error={Boolean(touched.email && errors.email)}
                                    />
                                </div>

                                <div className="form-group">
                                    <TextField
                                        type="firstName"
                                        label="Име"
                                        margin="normal"
                                        fullWidth={true}
                                        name="firstName"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.firstName}
                                        helperText={touched.firstName && errors.firstName}
                                        error={Boolean(touched.firstName && errors.firstName)}
                                    />
                                </div>

                                <div className="form-group">
                                    <TextField
                                        type="lastName"
                                        label="Презиме"
                                        margin="normal"
                                        fullWidth={true}
                                        name="lastName"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.lastName}
                                        helperText={touched.lastName && errors.lastName}
                                        error={Boolean(touched.lastName && errors.lastName)}
                                    />
                                </div>

                                <div className="form-group">
                                    <TextField
                                        type="text"
                                        label="Лозинка"
                                        margin="normal"
                                        fullWidth={true}
                                        name="password1"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.password1}
                                        helperText={touched.password1 && errors.password1}
                                        error={Boolean(touched.password1 && errors.password1)}
                                    />
                                </div>

                                <div className="form-group">
                                    <TextField
                                        type="text"
                                        margin="normal"
                                        label="Повтори ја лозинката"
                                        className="kt-width-full"
                                        name="password2"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.password2}
                                        helperText={touched.password2 && errors.password2}
                                        error={Boolean(
                                            touched.password2 && errors.password2
                                        )}
                                    />
                                </div>

                                {
                                    props.user.UserRole.roleId === 3
                                        ? <div className="form-group">
                                            <TextField
                                                name="role"
                                                id="filled-select-currency-native"
                                                select
                                                label="Улога (роља)"
                                                SelectProps={{
                                                    native: true,
                                                }}
                                                helperText={touched.role && errors.role}
                                                margin="normal"
                                                className="kt-width-full"
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                                value={values.role}
                                                error={Boolean(
                                                    touched.role && errors.role
                                                )}
                                            >
                                                {
                                                    userRoles.map(option => (
                                                        <option key={option.value} value={option.value}>
                                                            {option.label}
                                                        </option>
                                                    ))
                                                }
                                            </TextField>
                                        </div>
                                        : null
                                }

                                <div className="kt-login__actions">
                                    <Link to="/dashboard">
                                        <button
                                            type="button"
                                            className="btn btn-secondary btn-elevate kt-login__btn-secondary"
                                        >
                                            Назад
                                            </button>
                                    </Link>

                                    <button
                                        type="submit"
                                        style={{ marginLeft: '10px' }}
                                        className={
                                            `btn btn-primary btn-elevate kt-login__btn-primary ${clsx(
                                                {
                                                    "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": loading
                                                }
                                            )}`
                                        }
                                        disabled={isSubmitting}
                                    >
                                        Поднеси
                                        </button>
                                </div>
                            </form>
                        )}
                </Formik>
            </div>

        </div>
    );
}

const mapStateToProps = ({ auth: { user } }) => ({
    user
});

export default connect(mapStateToProps, auth.actions)(CreateUser);