import React, { useState, useEffect } from "react";
import { Formik } from "formik";
import { connect } from "react-redux";
import { TextField, Breadcrumbs } from "@material-ui/core";
import clsx from "clsx";

import { Link } from "react-router-dom";
import * as auth from "../../../store/ducks/auth.duck";
import { getPrasalnikMajById as getPrasalnikMajByIdAdmin } from "../../../crud/admin.crud";
import { changePrasalnikMajById, getPrasalnikMajById as getPrasalnikMajByIdSuperadmin } from "../../../crud/superadmin.crud";


function EditMajPrasalnik(props) {

    const [loading, setLoading] = useState(false);
    const [initPrashalnik, setInitPrashalnik] = useState({});
    const [loadingButtonStyle, setLoadingButtonStyle] = useState({
        paddingRight: "2.5rem"
    });

    const enableLoading = () => {
        setLoading(true);
        setLoadingButtonStyle({ paddingRight: "3.5rem" });
    };

    const disableLoading = () => {
        setLoading(false);
        setLoadingButtonStyle({ paddingRight: "2.5rem" });
    };

    const prasalnikId = Number(props.match.params.prasalnikId);

    let majPrasalnikModel = {
        dms_farmer_br_alp1: '',
        dms_full_name_al1: '',
        dms_adresa_al1: '',
        dms_tehnicar_al1: '',
        dms_dog_povrs_al1: '',
        dms_tut_pro_or_kg: '',
        dms_kof_tip_poseta_ind_1: '',
        dms_kof_brs_1: '',
        dms_kof_tip_poseta_grp_1: '',
        dms_kof_drs_1: '',
        dms_kkf_pcenka: '',
        dms_kkf_bostan: '',
        dms_kkf_zitarki: '',
        dms_kkf_piperki: '',
        dms_kkf_kompiri: '',
        dms_kkf_domati: '',
        dms_kkf_ovosje: '',
        dms_kkf_kromid: '',
        dms_kkf_drugo: '',
        dms_hdp_pcenka: '',
        dms_hdp_bostan: '',
        dms_hdp_zitarki: '',
        dms_hdp_piperki: '',
        dms_hdp_kompiri: '',
        dms_hdp_domati: '',
        dms_hdp_ovosje: '',
        dms_hdp_kromid: '',
        dms_hdp_drugo: '',
        dms_dogovor_usmen_argat_1: '',
        dms_ekipa_argat_1: '',
        dms_ucinik_argat_1: '',
        dms_casovo_argat_1: '',
        dms_den_argat_1: '',
        dms_nedelno_argat: '',
        dms_mesec_argat_1: '',
        dms_sezonakraj_argat_1: '',
        dms_lfa_nad_18: '',
        dms_lfa_15_17: '',
        dms_lfa_6_14: '',
        dms_lfa_0_5: '',
        dms_lfa_vkupno: '',
        dms_pna_6_14: '',
        dms_pna_15_17: '',
        dms_pna_nad_18: '',
        dms_pna_ziveat_farma: '',
        dms_lfm_nad_18: '',
        dms_lfm_15_17: '',
        dms_lfm_6_14: '',
        dms_lfm_0_5: '',
        dms_lfm_vkupno: '',
        dms_lfm_vozrasni: '',
        dms_lfm_ziveat_farma: '',
        dms_nzd_farmer_1: '',
        dms_voda_pienje_farmer_1: '',
        dms_voda_perenjde_farmer_1: '',
        dms_prostor_gotvenje_farmer_1: '',
        dms_ispravni_toaleti_farmer_1: '',
        dms_elener_farmer_1: '',
        dms_nzd_rabotnik_1: '',
        dms_voda_pienje_rabotnik_1: '',
        dms_voda_perenjde_rabotnik_1: '',
        dms_prostor_gotvenje_rabotnik_1: '',
        dms_ispravni_toaleti_rabotnik_1: '',
        dms_elener_rabotnik_1: '',
        dms_presaduvanje_12: '',
        dms_presaduvanje_13_14: '',
        dms_presaduvanje_15_17: '',
        dms_odgleduvanje_12: '',
        dms_odgleduvanje_13_14: '',
        dms_odgleduvanje_15_17: '',
        dms_nagjubruvanje_12: '',
        dms_nagjubruvanje_13_14: '',
        dms_nagjubruvanje_15_17: '',
        dms_cpapodgotovka_12: '',
        dms_cpapodgotovka_13_14: '',
        dms_cpapodgotovka_15_17: '',
        dms_cpaprskanje_12: '',
        dms_cpaprskanje_13_14: '',
        dms_cpaprskanje_15_17: '',
        dms_plevenje_12: '',
        dms_plevenje_13_14: '',
        dms_plevenje_15_17: '',
        dms_presaduvanje_rfa_13_14: '',
        dms_presaduvanje_rfa_15_17: '',
        dms_odgleduvanje_rfa_13_14: '',
        dms_odgleduvanje_rfa_15_17: '',
        dms_nagjubruvanje_rfa_13_14: '',
        dms_nagjubruvanje_rfa_15_17: '',
        dms_cpapodgotovka_rfa_13_14: '',
        dms_cpapodgotovka_rfa_15_17: '',
        dms_cpaprskanje_rfa_13_14: '',
        dms_cpaprskanje_rfa_15_17: '',
        dms_plevenje_rfa_13_14: '',
        dms_plevenje_rfa_15_17: '',
        dms_presaduvanje_rfm_13_14: '',
        dms_presaduvanje_rfm_15_17: '',
        dms_odgleduvanje_rfm_13_14: '',
        dms_odgleduvanje_rfm_15_17: '',
        dms_nagjubruvanje_rfm_13_14: '',
        dms_nagjubruvanje_rfm_15_17: '',
        dms_cpapodgotovka_rfm_13_14: '',
        dms_cpapodgotovka_rfm_15_17: '',
        dms_cpaprskanje_rfm_13_14: '',
        dms_cpaprskanje_rfm_15_17: '',
        dms_plevenje_rfm_13_14: '',
        dms_plevenje_rfm_15_17: '',
        dms_datum_tip_gjubrivo1: '',
        dms_tip_gjubrivo_osum22: '',
        dms_tip_gjubrivo_osum16: '',
        dms_tip_gjubrivo_deset20: '',
        dms_tip_gjubrivo_petnaeset15: '',
        dms_tip_gjubrivo_arsko: '',
        dms_novoposeana_kp_1: '',
        dms_predkultura_tutun: '',
        dms_predkultura_zelencuk: '',
        dms_predkultura_zito: '',
        dms_predkultura_drugo: '',
        dms_plodored_1: '',
        dms_br_oranja: '',
        dms_doza_tip_gjubrivo1: '',
        dms_rasaduvanje_nacin: '',
        dms_10maj: '',
        dms_20maj: '',
        dms_10juni: '',
        dms_po10juni: '',
        dms_30maj: '',
        dms_rastenija_na_dekar_16000: '',
        dms_rastenija_na_dekar_nad18000: '',
        dms_rastenija_na_dekar_18000: '',
        dms_dadeni_instrukcii_maj: '',
    }

    useEffect(() => {
        (async () => {
            const prashalnik = props.user.UserRole.roleId === 3 ? await getPrasalnikMajByIdSuperadmin({ id: prasalnikId }) : await getPrasalnikMajByIdAdmin({ id: prasalnikId });
            majPrasalnikModel = prashalnik.data.prasalnik;
            majPrasalnikModel.potpis = majPrasalnikModel.potpisImage;
            delete majPrasalnikModel.potpisImage;

            setInitPrashalnik(majPrasalnikModel);
        })();
    }, {});

    const PrashalnkikMarFields = ({
        values,
        status,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
    }) => {
        let fields = [];
        for (const key in majPrasalnikModel) {
            fields.push(<div className="form-group">
                <TextField
                    InputLabelProps={{ shrink: true }}
                    type="text"
                    label={`${key}`}
                    margin="normal"
                    fullWidth={true}
                    name={`${key}`}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values[key]}
                    helperText={touched[key] && errors[key]}
                    error={Boolean(touched[key] && errors[key])}
                />
            </div>);
        }
        return fields;
    }

    return (
        <div className="kt-portlet">
            <div className="kt-portlet__head">
                <Breadcrumbs aria-label="Breadcrumb" style={{ marginTop: '17px', fontSize: '18px' }}>
                    <Link color="inherit" to="/users" >
                        Комитенти
                    </Link>
                    <Link color="inherit" to={`/komitent/${props.match.params.komitentId}`} >
                        Комитент
                    </Link>
                    <div color="textPrimary">
                        Едитирање на прашалник Мај
                    </div>
                </Breadcrumbs>
            </div>

            <div className='kt-portlet__body'>

                <Formik
                    style={{ backgroundColor: 'white' }}
                    initialValues={{ ...initPrashalnik, id: prasalnikId }}
                    enableReinitialize={true}
                    onSubmit={
                        (values, { setStatus, setSubmitting }) => {
                            enableLoading();
                            setSubmitting(true);
                            changePrasalnikMajById(values)
                                .then((result) => {

                                    disableLoading();
                                    setSubmitting(false);
                                    setStatus('');

                                    props.history.push('/komitenti');
                                })
                                .catch((error) => {
                                    console.log({ error })
                                    disableLoading();
                                    setSubmitting(false);
                                    setStatus('Something went wrong!');
                                });
                        }
                    }
                >
                    {({
                        values,
                        status,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting,
                    }) => (
                            <form onSubmit={handleSubmit} className="kt-form">
                                {status && (
                                    <div role="alert" className="alert alert-danger">
                                        <div className="alert-text">{status}</div>
                                    </div>
                                )}

                                {
                                    PrashalnkikMarFields({
                                        values,
                                        status,
                                        errors,
                                        touched,
                                        handleChange,
                                        handleBlur,
                                        handleSubmit,
                                        isSubmitting,
                                    }).map((e) => {
                                        return e
                                    })
                                }

                                <div style={{ display: 'flex', flexDirection: 'column', marginBottom: '20px' }}>
                                    <label>Потпис</label>
                                    <img width="200" src={initPrashalnik.potpis} />
                                </div>

                                <div className="kt-login__actions">
                                    <Link to="/dashboard">
                                        <button
                                            type="button"
                                            className="btn btn-secondary btn-elevate kt-login__btn-secondary"
                                        >
                                            Назад
                                        </button>
                                    </Link>

                                    <button
                                        type="submit"
                                        style={{ marginLeft: '10px' }}
                                        className={
                                            `btn btn-primary btn-elevate kt-login__btn-primary ${clsx(
                                                {
                                                    "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": loading
                                                }
                                            )}`
                                        }
                                        disabled={isSubmitting}
                                    >
                                        Поднеси
                                        </button>
                                </div>
                            </form>
                        )}
                </Formik>
            </div>

        </div >
    );
}

const mapStateToProps = ({ auth: { user } }) => ({
    user
});

export default connect(mapStateToProps, auth.actions)(EditMajPrasalnik);