import React, { useState, useEffect } from "react";
import { Formik } from "formik";
import { connect } from "react-redux";
import { TextField, Breadcrumbs } from "@material-ui/core";
import clsx from "clsx";

import { Link } from "react-router-dom";
import * as auth from "../../../store/ducks/auth.duck";
import { getPrasalnikMartById as getPrasalnikMartByIdAdmin } from "../../../crud/admin.crud";
import { changePrasalnikMartById, getPrasalnikMartById as getPrasalnikMartByIdSuperadmin } from "../../../crud/superadmin.crud";

function EditMartPrasalnik(props) {

    const [loading, setLoading] = useState(false);
    const [initPrashalnik, setInitPrashalnik] = useState({});
    const [loadingButtonStyle, setLoadingButtonStyle] = useState({
        paddingRight: "2.5rem"
    });

    const enableLoading = () => {
        setLoading(true);
        setLoadingButtonStyle({ paddingRight: "3.5rem" });
    };

    const disableLoading = () => {
        setLoading(false);
        setLoadingButtonStyle({ paddingRight: "2.5rem" });
    };

    const prasalnikId = Number(props.match.params.prasalnikId);

    let martPrasalnikModel = {
        dms_farmer_br_alp1: '',
        dms_full_name_al1: '',
        dms_adresa_al1: '',
        dms_tehnicar_al1: '',
        dms_dog_povrs_al1: '',
        dms_tut_pro_or_kg: '',
        dms_povrs_sop_al1: '',
        dms_povrs_naem_al1: '',
        dms_povrs_ops_al1: '',
        dms_pirc_nabljuduvanje_detali1_prd: '',
        dms_pirc_nabljuduvanje_detali1_pis: '',
        dms_tip_poseta_ind_al1: '',
        dms_tip_poseta_grp_al1: '',
        dms_site_prin_al1: '',
        dms_vkupno_voz_al1: '',
        dms_vkupno_deca17_al1: '',
        dms_vkupno_deca14_al1: '',
        dms_vkupno_deca5_al1: '',
        dms_vkupno_voz1_al1: '',
        dms_pos_n_deca_14_al1: '',
        dms_pos_n_deca_17_al1: '',
        dms_pos_n_deca_18_al1: '',
        dms_ziveat_farm_al1: '',
        dms_sprav_lea_al1: '',
        dms_sprav_lea14_al1: '',
        dms_sprav_lea17_al1: '',
        dms_podg_zem_al1: '',
        dms_podg_zem14_al1: '',
        dms_podg_zem17_al1: '',
        dms_dom_zd_al1: '',
        dms_dom_zd14_al1: '',
        dms_dom_zd17_al1: '',
        dms_odg_hr_al1: '',
        dms_odg_hr14_al1: '',
        dms_odg_hr17_al1: '',
        dms_odg_ziv_al1: '',
        dms_odg_ziv14_al1: '',
        dms_odg_ziv17_al1: '',
        dms_sprav_lea14_arg_al1: '',
        dms_sprav_lea17_arg_al1: '',
        dms_podg_zem_arg14_al1: '',
        dms_podg_zem_arg17_al1: '',
        dms_sprav_lea_mig14_al1: '',
        dms_sprav_lea_mig17_al1: '',
        dms_podg_zem_mig14_al1: '',
        dms_podg_zem_mig17_al1: '',
        dms_tip_na_tutun: '',
        dms_druga_sorta: '',
        dms_sertificirano: '',
        dms_br_na_lei: '',
        dms_lei_m2: '',
        dms_datum_na_seidba: '',
        dms_upotreba_na_seme_gr_m2: '',
        dms_otvoreno: '',
        dms_zatvoreno: '',
        dms_propisana_golemina: '',
        dms_sandace_za_preparati: '',
        dms_zaoruvanje_rasadnik: '',
        dms_prihrana1_tarana: '',
        dms_prihrana1_kristalno: '',
        dms_prihrana1_organsko: '',
        dms_prihrana1_petnaeset: '',
        dms_prihrana1_drugo: '',
        dms_prihrana1_doza: '',
        dms_prihrana1_1_doza: '',
        dms_prihrana2_tarana: '',
        dms_prihrana2_kristalno: '',
        dms_prihrana2_organsko: '',
        dms_prihrana2_petnaeset: '',
        dms_prihrana2_drugo: '',
        dms_prihrana2_doza: '',
        dms_prihrana2_2_doza: '',
        dms_prihrana3_tarana: '',
        dms_prihrana3_kristalno: '',
        dms_prihrana3_organsko: '',
        dms_prihrana3_petnaeset: '',
        dms_prihrana3_drugo: '',
        dms_prihrana3_doza: '',
        dms_prihrana3_2_doza: '',
        dms_prihrana_1_datum: '',
        dms_prihrana_2_datum: '',
        dms_prihrana_2_datum: '',
        dms_gustina_na_rasad_normalen: '',
        dms_gustina_na_rasad_redok: '',
        dms_gustina_na_rasad_gust: '',
        dms_izrabotka_na_lei_dobra: '',
        dms_izrabotka_na_lei_sredna: '',
        dms_izrabotka_na_lei_losa: '',
        dms_zatrevenost_nema: '',
        dms_zatreveno_mala: '',
        dms_zatrevenost_golema: '',
        dms_provetruvanje_dobro: '',
        dms_provetruvanje_sredno: '',
        dms_provetruvanje_loso: '',
        dms_navodnuvanje_normalno: '',
        dms_navodnuvanje_malku: '',
        dms_navodnuvanje_prekumerno: '',
        dms_higiena_vo_lei_cisto: '',
        dms_higiena_vo_lei_prosecno: '',
        dms_higiena_vo_lei_necisto: '',
        dms_pleveli: '',
        dms_ras_pleveli_doza: '',
        dms_ras_pleveli_nacin: '',
        dms_ras_pleveli_datum: '',
        dms_ras_fungicid_doza: '',
        dms_ras_fungicid_nacin: '',
        dms_ras_insekticid_doza: '',
        dms_ras_insekticid_nacin: '',
        dms_ras_fungicid_datum: '',
        dms_ras_insekticid_datum: '',
        dms_pesticidi_dervinol_mart: '',
        dms_pesticidi_gamit_mart: '',
        dms_pesticidi_stomp_mart: '',
        dms_pesticidi_micro_mart: '',
        dms_pesticidi_meteor_mart: '',
        dms_pleveli_fungicid_preventivno_mart: '',
        dms_pleveli_fungicid_secenje_mart: '',
        dms_pleveli_fungicid_plamenica_mart: '',
        dms_pleveli_fungicid_voski_mart: '',
        dms_pleveli_fungicid_pepelnica_mart: '',
        dms_pesticidi_dithane_mart: '',
        dms_pesticidi_manfil_mart: '',
        dms_pesticidi_orvego_mart: '',
        dms_pesticidi_penncozeb_mart: '',
        dms_pesticidi_previcur_mart: '',
        dms_pesticidi_proplant_mart: '',
        dms_pesticidi_ridomil_mart: '',
        dms_pesticidi_ridosan_mart: '',
        dms_pesticidi_signum_mart: '',
        dms_pesticidi_topas_mart: '',
        dms_pesticidi_topsin_mart: '',
        dms_pesticidi_infinito_mart: '',
        dms_pleveli_insekticid_preventivno_mart: '',
        dms_pleveli_insekticid_secenje_mart: '',
        dms_pleveli_insekticid_plamenica_mart: '',
        dms_pleveli_insekticid_voski_mart: '',
        dms_pleveli_insekticid_pepelnica_mart: '',
        dms_pesticidi_actara_mart: '',
        dms_pesticidi_confidor_mart: '',
        dms_pesticidi_decis_mart: '',
        dms_pesticidi_imidor_mart: '',
        dms_pesticidi_karate_mart: '',
        dms_pesticidi_kohinor_mart: '',
        dms_pesticidi_mospilan_mart: '',
        dms_pesticidi_nuprid_mart: '',
        dms_pesticidi_premier_mart: '',
        dms_pesticidi_tornado_mart: '',
        dms_dadeni_instrukcii: '',
    }

    useEffect(() => {
        (async () => {
            const prashalnik = props.user.UserRole.roleId === 3
                ? await getPrasalnikMartByIdSuperadmin({ id: prasalnikId })
                : await getPrasalnikMartByIdAdmin({ id: prasalnikId });

            martPrasalnikModel = prashalnik.data.prasalnik;
            martPrasalnikModel.potpis = martPrasalnikModel.potpisImage;
            delete martPrasalnikModel.potpisImage;

            setInitPrashalnik(martPrasalnikModel);
        })();
    }, {});

    const PrashalnkikMarFields = ({
        values,
        status,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
    }) => {
        let fields = [];
        for (const key in martPrasalnikModel) {
            fields.push(<div className="form-group">
                <TextField
                    InputLabelProps={{ shrink: true }}
                    type="text"
                    label={`${key}`}
                    margin="normal"
                    fullWidth={true}
                    name={`${key}`}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values[key]}
                    helperText={touched[key] && errors[key]}
                    error={Boolean(touched[key] && errors[key])}
                />
            </div>);
        }
        return fields;
    }

    return (
        <div className="kt-portlet">
            <div className="kt-portlet__head">
                <Breadcrumbs aria-label="Breadcrumb" style={{ marginTop: '17px', fontSize: '18px' }}>
                    <Link color="inherit" to="/users" >
                        Комитенти
                    </Link>
                    <Link color="inherit" to={`/komitent/${props.match.params.komitentId}`} >
                        Комитент
                    </Link>
                    <div color="textPrimary">
                        Едитирање на прашалник Март
                    </div>
                </Breadcrumbs>
            </div>

            <div className='kt-portlet__body'>

                <Formik
                    style={{ backgroundColor: 'white' }}
                    initialValues={{ ...initPrashalnik, id: prasalnikId }}
                    enableReinitialize={true}
                    onSubmit={
                        (values, { setStatus, setSubmitting }) => {
                            enableLoading();
                            setSubmitting(true);

                            changePrasalnikMartById(values).then((result) => {

                                disableLoading();
                                setSubmitting(false);
                                setStatus('');

                                props.history.push('/komitenti');
                            })
                                .catch((error) => {
                                    console.log({ error })
                                    disableLoading();
                                    setSubmitting(false);
                                    setStatus('Something went wrong!');
                                });
                        }
                    }
                >
                    {({
                        values,
                        status,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting,
                    }) => (
                            <form onSubmit={handleSubmit} className="kt-form">
                                {
                                    status && (
                                        <div role="alert" className="alert alert-danger">
                                            <div className="alert-text">{status}</div>
                                        </div>
                                    )
                                }

                                {
                                    PrashalnkikMarFields({
                                        values,
                                        status,
                                        errors,
                                        touched,
                                        handleChange,
                                        handleBlur,
                                        handleSubmit,
                                        isSubmitting,
                                    }).map((e) => {
                                        return e
                                    })
                                }

                                <div style={{ display: 'flex', flexDirection: 'column', marginBottom: '20px' }}>
                                    <label>Потпис</label>
                                    <img width="200" src={initPrashalnik.potpis} />
                                </div>

                                <div className="kt-login__actions">
                                    <Link to="/dashboard">
                                        <button
                                            type="button"
                                            className="btn btn-secondary btn-elevate kt-login__btn-secondary"
                                        >
                                            Назад
                                        </button>
                                    </Link>

                                    {
                                        props.user.UserRole.roleId === 3
                                            ? <button
                                                type="submit"
                                                style={{ marginLeft: '10px' }}
                                                className={
                                                    `btn btn-primary btn-elevate kt-login__btn-primary ${clsx(
                                                        {
                                                            "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": loading
                                                        }
                                                    )}`
                                                }
                                                disabled={isSubmitting}
                                            >
                                                Поднеси
                                        </button>
                                            : null
                                    }

                                </div>
                            </form>
                        )}
                </Formik>
            </div>

        </div >
    );
}

const mapStateToProps = ({ auth: { user } }) => ({
    user
});

export default connect(mapStateToProps, auth.actions)(EditMartPrasalnik);