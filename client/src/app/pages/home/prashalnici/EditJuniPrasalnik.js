import React, { useState, useEffect } from "react";
import { Formik } from "formik";
import { connect } from "react-redux";
import { TextField, Breadcrumbs } from "@material-ui/core";
import clsx from "clsx";

import { Link } from "react-router-dom";
import * as auth from "../../../store/ducks/auth.duck";
import { getPrasalnikJuniById as getPrasalnikJuniByIdAdmin } from "../../../crud/admin.crud";
import { getPrasalnikJuniById as getPrasalnikJuniByIdSuperadmin, changePrasalnikJuniById } from "../../../crud/superadmin.crud";

function EditJuniPrasalnik(props) {

    const [loading, setLoading] = useState(false);
    const [initPrashalnik, setInitPrashalnik] = useState({});
    const [loadingButtonStyle, setLoadingButtonStyle] = useState({
        paddingRight: "2.5rem"
    });

    const enableLoading = () => {
        setLoading(true);
        setLoadingButtonStyle({ paddingRight: "3.5rem" });
    };

    const disableLoading = () => {
        setLoading(false);
        setLoadingButtonStyle({ paddingRight: "2.5rem" });
    };

    const prasalnikId = Number(props.match.params.prasalnikId);

    let juniPrasalnikModel = {
        dms_farmer_br_alp1: '',
        dms_full_name_al1: '',
        dms_adresa_al1: '',
        dms_tehnicar_al1: '',
        dms_dog_povrs_al1: '',
        dms_tut_pro_or_kg: '',
        dms_kof_jun_tip_poseta_ind: '',
        dms_kof_jun_tip_poseta_grp: '',
        dms_kof_jun_detska_sila: '',
        dms_kof_jun_prihod_casa: '',
        dms_kof_jun_bez_sredina: '',
        dms_kof_jun_soglasno_zakon: '',
        dms_kof_jun_fer_tretman: '',
        dms_kof_jun_prinudna_rab: '',
        dms_kof_jun_slob_zdruzuvanje: '',
        dms_fam_12_berenje: '',
        dms_fam_13_14_berenje: '',
        dms_fam_15_17_berenje: '',
        dms_fam_12_polnenje: '',
        dms_fam_13_14_polnenje: '',
        dms_fam_15_17_polnenje: '',
        dms_arg_12_berenje: '',
        dms_arg_13_14_berenje: '',
        dms_arg_15_17_berenje: '',
        dms_arg_12_polnenje: '',
        dms_arg_13_14_polnenje: '',
        dms_arg_15_17_polnenje: '',
        dms_mig_12_berenje: '',
        dms_mig_13_14_berenje: '',
        dms_mig_15_17_berenje: '',
        dms_mig_12_polnenje: '',
        dms_mig_13_14_polnenje: '',
        dms_mig_15_17_polnenje: '',
        dms_kof_jun_dat_treta_poseta: '',
        dms_kopanje_dat_rm_jun: '',
        dms_kopanje_rm_jun: '',
        dms_kopanje_dat_mr_jun: '',
        dms_kopanje_mr_jun: '',
        dms_naklon_niva_jun: '',
        dms_procent_prifatenost_jun: '',
        dms_nav_prv_cas_jun: '',
        dms_nav_prv_prskac_jun: '',
        dms_nav_prv_brazdi_jun: '',
        dms_nav_vto_cas_jun: '',
        dms_nav_vto_prskac_jun: '',
        dms_nav_vto_brazdi_jun: '',
        dms_nav_tret_cas_jun: '',
        dms_nav_tret_prskac_jun: '',
        dms_nav_tret_brazdi_jun: '',
        dms_nema_nav_jun: '',
        dms_priprema_pesticid_jun: '',
        dms_ispravnost_prskalki: '',
        dms_4_dela: '',
        dms_mantil: '',
        dms_maska: '',
        dms_naocari: '',
        dms_rakavici: '',
        dms_bol_ste_plev_jun: '',
        dms_pleveli_datum_jun: '',
        dms_pleveli_doza_jun: '',
        dms_pleveli_nacin_jun: '',
        dms_up_pes_pleveli_devrinol_jun: '',
        dms_up_pes_pleveli_gamit_jun: '',
        dms_up_pes_pleveli_stomp_jun: '',
        dms_up_pes_pleveli_most_micro_jun: '',
        dms_up_pes_pleveli_meteor_jun: '',
        dms_up_pes_pleveli_dual_gold_jun: '',
        dms_up_pes_pleveli_fusilade_jun: '',
        dms_up_pes_pleveli_pendigan_jun: '',
        dms_bsp_fungicid_preventivno_jun: '',
        dms_bsp_fungicid_secenje_jun: '',
        dms_bsp_fungicid_plamenica_jun: '',
        dms_bsp_fungicid_trips_jun: '',
        dms_bsp_fungicid_pepelnica_jun: '',
        dms_bsp_fungicid_voska_jun: '',
        dms_bsp_fungicid_bolva_jun: '',
        dms_bsp_fungicid_kafena_jun: '',
        dms_bsp_fungicid_zabino_oko_jun: '',
        dms_up_fun_dithane_jun: '',
        dms_up_fun_manfil_jun: '',
        dms_up_fun_orvego_jun: '',
        dms_up_fun_penkozeb_jun: '',
        dms_up_fun_previkur_jun: '',
        dms_up_fun_proplant_jun: '',
        dms_up_fun_ridomil_jun: '',
        dms_up_fun_ridosan_jun: '',
        dms_up_fun_signum_jun: '',
        dms_up_fun_topas_jun: '',
        dms_up_fun_topsin_jun: '',
        dms_up_fun_acrobat_jun: '',
        dms_up_fun_armetil_jun: '',
        dms_up_fun_matador_jun: '',
        dms_up_fun_tilt_jun: '',
        dms_fungicid_datum_jun: '',
        dms_fungicid_doza_jun: '',
        dms_fungicid_nacin_jun: '',
        dms_bsp_insekticid_preventivno_jun: '',
        dms_bsp_insekticid_secenje_jun: '',
        dms_bsp_insekticid_plamenica_jun: '',
        dms_bsp_insekticid_trips_jun: '',
        dms_bsp_insekticid_pepelnica_jun: '',
        dms_bsp_insekticid_voska_jun: '',
        dms_bsp_insekticid_bolva_jun: '',
        dms_bsp_insekticid_kafena_jun: '',
        dms_bsp_insekticid_zabino_oko_jun: '',
        dms_up_ins_actara_jun: '',
        dms_up_ins_confidor_jun: '',
        dms_up_ins_decis_jun: '',
        dms_up_ins_imidor_jun: '',
        dms_up_ins_karate_jun: '',
        dms_up_ins_kohinor_jun: '',
        dms_up_ins_mospilan_jun: '',
        dms_up_ins_nuprid_jun: '',
        dms_up_ins_premier_jun: '',
        dms_up_ins_tornado_jun: '',
        dms_up_ins_affirm_jun: '',
        dms_up_ins_avaunt_jun: '',
        dms_up_ins_belt_jun: '',
        dms_up_ins_chess_jun: '',
        dms_up_ins_direkt_jun: '',
        dms_up_ins_laser_jun: '',
        dms_up_ins_movento_jun: '',
        dms_up_ins_nurele_jun: '',
        dms_up_ins_teppeki_jun: '',
        dms_insekticid_datum_jun: '',
        dms_insekticid_doza_jun: '',
        dms_insekticid_nacin_jun: '',
        dms_dadeni_instrukcii_juni: '',
    }

    useEffect(() => {
        (async () => {
            const prashalnik = props.user.UserRole.roleId === 3 ? await getPrasalnikJuniByIdSuperadmin({ id: prasalnikId }) : await getPrasalnikJuniByIdAdmin({ id: prasalnikId });
            
            juniPrasalnikModel = prashalnik.data.prasalnik;
            juniPrasalnikModel.potpis = juniPrasalnikModel.potpisImage;

            delete juniPrasalnikModel.potpisImage;

            setInitPrashalnik(juniPrasalnikModel);
        })();
    }, {});

    const PrashalnkikMarFields = ({
        values,
        status,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
    }) => {
        let fields = [];
        for (const key in juniPrasalnikModel) {
            fields.push(<div className="form-group">
                <TextField
                    InputLabelProps={{ shrink: true }}
                    type="text"
                    label={`${key}`}
                    margin="normal"
                    fullWidth={true}
                    name={`${key}`}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    value={values[key]}
                    helperText={touched[key] && errors[key]}
                    error={Boolean(touched[key] && errors[key])}
                />
            </div>);
        }
        return fields;
    }

    return (
        <div className="kt-portlet">
            <div className="kt-portlet__head">
                <Breadcrumbs aria-label="Breadcrumb" style={{ marginTop: '17px', fontSize: '18px' }}>
                    <Link color="inherit" to="/users" >
                        Комитенти
                    </Link>
                    <Link color="inherit" to={`/komitent/${props.match.params.komitentId}`} >
                        Комитент
                    </Link>
                    <div color="textPrimary">
                        Едитирање на прашалник Јуни
                    </div>
                </Breadcrumbs>
            </div>

            <div className='kt-portlet__body'>

                <Formik
                    style={{ backgroundColor: 'white' }}
                    initialValues={{ ...initPrashalnik, id: prasalnikId }}
                    enableReinitialize={true}
                    onSubmit={
                        (values, { setStatus, setSubmitting }) => {
                            enableLoading();
                            setSubmitting(true);
                            changePrasalnikJuniById(values)
                                .then((result) => {

                                    disableLoading();
                                    setSubmitting(false);
                                    setStatus('');

                                    props.history.push('/komitenti');
                                })
                                .catch((error) => {
                                    console.log({ error })
                                    disableLoading();
                                    setSubmitting(false);
                                    setStatus('Something went wrong!');
                                });
                        }
                    }
                >
                    {({
                        values,
                        status,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting,
                    }) => (
                            <form onSubmit={handleSubmit} className="kt-form">
                                {status && (
                                    <div role="alert" className="alert alert-danger">
                                        <div className="alert-text">{status}</div>
                                    </div>
                                )}

                                {
                                    PrashalnkikMarFields({
                                        values,
                                        status,
                                        errors,
                                        touched,
                                        handleChange,
                                        handleBlur,
                                        handleSubmit,
                                        isSubmitting,
                                    }).map((e) => {
                                        return e
                                    })
                                }

                                <div style={{ display: 'flex', flexDirection: 'column', marginBottom: '20px' }}>
                                    <label>Потпис</label>
                                    <img width="200" src={initPrashalnik.potpis} />
                                </div>

                                <div className="kt-login__actions">
                                    <Link to="/dashboard">
                                        <button
                                            type="button"
                                            className="btn btn-secondary btn-elevate kt-login__btn-secondary"
                                        >
                                            Назад
                                        </button>
                                    </Link>

                                    <button
                                        type="submit"
                                        style={{ marginLeft: '10px' }}
                                        className={
                                            `btn btn-primary btn-elevate kt-login__btn-primary ${clsx(
                                                {
                                                    "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": loading
                                                }
                                            )}`
                                        }
                                        disabled={isSubmitting}
                                    >
                                        Поднеси
                                        </button>
                                </div>
                            </form>
                        )}
                </Formik>
            </div>

        </div >
    );
}

const mapStateToProps = ({ auth: { user } }) => ({
    user
});

export default connect(mapStateToProps, auth.actions)(EditJuniPrasalnik);