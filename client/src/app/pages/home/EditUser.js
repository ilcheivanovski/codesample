import React, { useState, useEffect } from "react";
import { Formik } from "formik";

import { connect } from "react-redux";
import * as auth from "../../store/ducks/auth.duck";

import { TextField, Breadcrumbs } from "@material-ui/core";
import clsx from "clsx";
import { Link } from "react-router-dom";

import { changeUsersProfile, getUserById } from "../../crud/superadmin.crud";
import { getTehnicharById, changeTehnicharProfile } from "../../crud/admin.crud";

const userRoles = [
    {
        value: 'user',
        label: 'Техничар',
    },
    {
        value: 'admin',
        label: 'Админ',
    },
];

function EditUser(props) {

    const [loading, setLoading] = useState(false);
    const [initUser, setInitUser] = useState({});
    const [loadingButtonStyle, setLoadingButtonStyle] = useState({
        paddingRight: "2.5rem"
    });

    const enableLoading = () => {
        setLoading(true);
        setLoadingButtonStyle({ paddingRight: "3.5rem" });
    };

    const disableLoading = () => {
        setLoading(false);
        setLoadingButtonStyle({ paddingRight: "2.5rem" });
    };

    const param = Number(props.match.params.userId);

    const userModel = {
        email: '',
        firstName: '',
        lastName: '',
        role: ''
    }

    const enumToAdminName = (roleId) => {
        switch (roleId) {
            case 1: {
                return 'user';
            }
            case 2: {
                return 'admin';
            }
            case 3: {
                return 'superadmin';
            }
            default:
                return null;
        }
    }

    useEffect(() => {
        (async () => {
            const user = props.user.UserRole.roleId === 3 ? await getUserById({ id: param }) : await getTehnicharById({ id: param });

            userModel.email = user.data.user.email;
            userModel.firstName = user.data.user.firstName;
            userModel.lastName = user.data.user.lastName;
            userModel.role = enumToAdminName(user.data.user.UserRole.roleId);

            setInitUser(userModel);
        })();
    }, {});

    return (
        <div className="kt-portlet">
            <div className="kt-portlet__head">
                <Breadcrumbs aria-label="Breadcrumb" style={{ marginTop: '17px', fontSize: '18px' }}>
                    <Link color="inherit" to="/users" >
                        Техничари - Админи
                    </Link>
                    <div color="textPrimary">
                        Едитирање на профил
                    </div>
                </Breadcrumbs>
            </div>

            <div className='kt-portlet__body'>

                <Formik
                    style={{ backgroundColor: 'white' }}
                    initialValues={{ ...initUser, id: param }}
                    enableReinitialize={true}
                    validate={values => {
                        const errors = {};

                        if (values.password1 !== values.password2) {
                            errors.password2 = "Лозинките мора да се совпаѓаат";
                        }

                        return errors;
                    }}
                    onSubmit={
                        (values, { setStatus, setSubmitting }) => {
                            enableLoading();
                            setSubmitting(true);

                            const promise = props.user.UserRole.roleId === 3
                                ? changeUsersProfile(values)
                                : changeTehnicharProfile(values);
                            promise
                                .then((result) => {

                                    disableLoading();
                                    setSubmitting(false);
                                    setStatus('');

                                    props.history.push('/users');
                                })
                                .catch((error) => {
                                    console.log({ error })
                                    disableLoading();
                                    setSubmitting(false);
                                    setStatus('Something went wrong!');
                                });
                        }
                    }
                >
                    {({
                        values,
                        status,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting,
                    }) => (
                            <form onSubmit={handleSubmit} className="kt-form">
                                {status && (
                                    <div role="alert" className="alert alert-danger">
                                        <div className="alert-text">{status}</div>
                                    </div>
                                )}

                                <div className="form-group">
                                    <TextField
                                        InputLabelProps={{ shrink: true }}
                                        type="email"
                                        label="Email"
                                        margin="normal"
                                        fullWidth={true}
                                        name="email"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.email}
                                        helperText={touched.email && errors.email}
                                        error={Boolean(touched.email && errors.email)}
                                    />
                                </div>

                                <div className="form-group">
                                    <TextField
                                        InputLabelProps={{ shrink: true }}
                                        type="firstName"
                                        label="Име"
                                        margin="normal"
                                        fullWidth={true}
                                        name="firstName"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.firstName}
                                        helperText={touched.firstName && errors.firstName}
                                        error={Boolean(touched.firstName && errors.firstName)}
                                    />
                                </div>

                                <div className="form-group">
                                    <TextField
                                        InputLabelProps={{ shrink: true }}
                                        type="lastName"
                                        label="Презиме"
                                        margin="normal"
                                        fullWidth={true}
                                        name="lastName"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.lastName}
                                        helperText={touched.lastName && errors.lastName}
                                        error={Boolean(touched.lastName && errors.lastName)}
                                    />
                                </div>

                                {
                                    props.user.UserRole.roleId === 3
                                        ? <div className="form-group">
                                            <TextField
                                                InputLabelProps={{ shrink: true }}
                                                id="filled-select-currency-native"
                                                select
                                                label="Улога (роља)"
                                                value={values.role}
                                                SelectProps={{
                                                    native: true,
                                                }}
                                                helperText="Ве молам одберете улога (роља)"
                                                margin="normal"
                                                className="kt-width-full"
                                                name="role"
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                                value={values.role}
                                                helperText={touched.role && errors.role}
                                            >
                                                {
                                                    userRoles.map(option => (
                                                        <option key={option.value} value={option.value}>
                                                            {option.label}
                                                        </option>
                                                    ))
                                                }
                                            </TextField>
                                        </div>
                                        : null
                                }

                                <div className="form-group">
                                    <TextField
                                        type="text"
                                        label="Лозинка"
                                        margin="normal"
                                        fullWidth={true}
                                        name="password1"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.password1}
                                        helperText={touched.password1 && errors.password1}
                                        error={Boolean(touched.password1 && errors.password1)}
                                    />
                                </div>

                                <div className="form-group">
                                    <TextField
                                        type="text"
                                        margin="normal"
                                        label="Повтори ја лозинката"
                                        className="kt-width-full"
                                        name="password2"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.password2}
                                        helperText={touched.password2 && errors.password2}
                                        error={Boolean(
                                            touched.password2 && errors.password2
                                        )}
                                    />
                                </div>

                                <div className="form-group">
                                    {
                                        initUser.role === 'user'
                                            ? <Link to={`/editUser/${param}/komitenti`} >
                                                <button type="button" className="btn btn-primary btn-elevate kt-login__btn-primary ">
                                                    Доделени Комитенти
                                                </button>
                                            </Link>
                                            : null
                                    }
                                </div>

                                <div className="kt-login__actions">
                                    <Link to="/dashboard">
                                        <button
                                            type="button"
                                            className="btn btn-secondary btn-elevate kt-login__btn-secondary"
                                        >
                                            Назад
                                            </button>
                                    </Link>

                                    <button
                                        type="submit"
                                        style={{ marginLeft: '10px' }}
                                        className={
                                            `btn btn-primary btn-elevate kt-login__btn-primary ${clsx(
                                                {
                                                    "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": loading
                                                }
                                            )}`
                                        }
                                        disabled={isSubmitting}
                                    >
                                        Поднеси
                                        </button>
                                </div>
                            </form>
                        )}
                </Formik>
            </div>

        </div >
    );
}

const mapStateToProps = ({ auth: { user } }) => ({
    user
});

export default connect(mapStateToProps, auth.actions)(EditUser);