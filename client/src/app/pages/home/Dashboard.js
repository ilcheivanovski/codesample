import React, { useMemo, useState, useEffect } from "react";
import {
  Portlet,
  PortletBody,
} from "../../partials/content/Portlet";
import QuickStatsChart from "../../widgets/QuickStatsChart";
import Notice from "../../partials/content/Notice";

import { connect } from "react-redux";
import * as auth from "../../store/ducks/auth.duck";

import { CircularProgress } from "@material-ui/core";

import {
  prashalniciCharts as prashalniciChartsAdmin,
} from "../../crud/admin.crud";
import {
  prashalniciCharts as prashalniciChartsSuperadmin,
} from "../../crud/superadmin.crud";

import moment from "moment";
import clsx from "clsx";

function Dashboard(props) {
  const [chartsData, setChartsData] = useState([]);
  const [loading, setLoading] = React.useState(true);
  const [loadingButton, setLoadingButton] = React.useState(false);


  const setChart = async () => {
    let chart = JSON.parse(localStorage.getItem('chart'));

    if (!chart) {
      const response = props.user.UserRole.roleId === 3 ? await prashalniciChartsSuperadmin() : await prashalniciChartsAdmin();

      chart = {
        charts: response.data.charts,
        timestamp: new Date(),
      }

      localStorage.setItem('chart', JSON.stringify({...chart}));
    }
    setChartsData(chart);
  }

  const chartsAsync = async () => {
    setLoading(true);
    await setChart();
    setLoading(false);
  }

  const updateCharts = async () => {
    localStorage.setItem('chart', null);

    setLoadingButton(true);
    await setChart();
    setLoadingButton(false);
  }

  useEffect(() => {
    chartsAsync();
  }, {});

  return (
    <>
      {chartsData.charts
        ? <Notice className="notice-main">
          <div className="azuriraj-text">
            <div>
              Последен пат земено податоци на:
            </div>
            <div>
              {moment(chartsData.timestamp).format('MM/DD/YYYY - HH:mm')}
            </div>
          </div>
          <button
            type="submit"
            className={
              `btn btn-primary btn-elevate kt-login__btn-primary azuriraj-button ${clsx(
                {
                  "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": loadingButton
                }
              )}`
            }
            disabled={loadingButton}
            onClick={async () => updateCharts()}
          >
            Ажурирај
          </button>
        </Notice>
        : null
      }

      <div className="row">
        <div className="col-xl-12">
          <div className="row adjusted-height">

            {loading ? <div className="circular-progress-dashboard"><CircularProgress /></div> : null}

            {
              chartsData.charts && chartsData.charts.map((chart, index) => {
                return (
                  <div className="col-sm-12 col-md-12 col-lg-6 height-100" key={index}>
                    <Portlet className="charts-portlet kt-portlet--border-bottom-brand height-100">
                      <PortletBody fluid={true}>
                        <QuickStatsChart
                          value={chart.name}
                          desc="Пополнети (%)"
                          data={{
                            labels: [
                              'Пополнето',
                              'Непополнето',
                            ],
                            datasets: [{
                              data: [chart.zatvoreniProcent, chart.otvoreniProcent],
                              backgroundColor: [
                                "#36A2EB",
                                "#f03e2e",
                              ],
                            }],
                          }}
                        />
                      </PortletBody>
                    </Portlet>
                  </div>
                )
              })
            }

          </div>
        </div>
      </div>

    </>
  );
}

const mapStateToProps = ({ auth: { user } }) => ({
  user
});

export default connect(mapStateToProps, auth.actions)(Dashboard);