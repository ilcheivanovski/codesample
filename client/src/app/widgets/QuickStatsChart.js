import React, { useEffect, useRef } from "react";
import { Chart } from "chart.js";
import { useSelector } from "react-redux";
import { metronic } from "../../_metronic";

export default function QuickStatsChart({
  value,
  desc,

  // array of numbers
  data,
  // chart line color
  color,
  // chart line size
  border
}) {
  const canvasRef = useRef();

  useEffect(() => {
    const config = {
      type: "doughnut",
      data,
      options: {
        responsive: true,
      }
    };

    const chart = new Chart(canvasRef.current, config);

    return () => {
      chart.destroy();
    };
  }, [data, color, border]);

  return (
    <div className="kt-widget26" >
      <div className="kt-widget26__content" style={{ height: '100px' }}>
        <span className="kt-widget26__number">{value}</span>
        <span className="kt-widget26__desc">{desc}</span>
      </div>

      <div
        className="kt-widget26__chart"
        style={{ width: "400px", height: "400px", left: '20%;' }}
      >
        <canvas ref={canvasRef} id="kt_chart_quick_stats_1" />
      </div>
    </div>
  );
}
