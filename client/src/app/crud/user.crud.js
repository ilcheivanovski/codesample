import axios from "axios";

const BASE_URL = process.env.REACT_APP_NODE_ENV === 'development' ? process.env.REACT_APP_PUBLIC_URL : window.location.origin;

// user
export const CHANGE_PROFILE_URL = `${BASE_URL}/user/changeProfile`;

// Komitenti
const KOMITENTI = `${BASE_URL}/user/komitenti`;
const GET_KOMITENT_BY_ID = `${BASE_URL}/user/getKomitentById`;

// GET PRASHALNICI
const GET_PRASHALNIK_MART_BY_ID = `${BASE_URL}/user/getPrasalnikMartById`;
const GET_PRASHALNIK_MAJ_BY_ID = `${BASE_URL}/user/getPrasalnikMajById`;
const GET_PRASHALNIK_JUNI_BY_ID = `${BASE_URL}/user/getPrasalnikJuniById`;
const GET_PRASHALNIK_AVGUST_BY_ID = `${BASE_URL}/user/getPrasalnikAvgustById`;

export function changeProfile(profile) {
    return axios.put(CHANGE_PROFILE_URL, profile);
}

// Komitenti
export function getKomitenti() {
    return axios.get(KOMITENTI);
}

export function getKomitentById(profile) {
    return axios.post(GET_KOMITENT_BY_ID, profile);
}

// GET PRASHALNICI
export function getPrasalnikMartById(profile) {
    return axios.post(GET_PRASHALNIK_MART_BY_ID, profile);
}

export function getPrasalnikMajById(profile) {
    return axios.post(GET_PRASHALNIK_MAJ_BY_ID, profile);
}

export function getPrasalnikJuniById(profile) {
    return axios.post(GET_PRASHALNIK_JUNI_BY_ID, profile);
}

export function getPrasalnikAvgustById(profile) {
    return axios.post(GET_PRASHALNIK_AVGUST_BY_ID, profile);
}