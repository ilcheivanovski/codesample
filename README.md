#client - /client/readme.md

- material-ui/core
- axios
- chart.js
- react-moment
- react-redux
- react-select
- reactstrap
- redux
- redux-persist
- redux-saga

#server - /server/readme.md

- @sendgrid/mail
- bcrypt-nodejs
- body-parser
- core-js
- cors
- express
- express-routes-mapper
- faker
- helmet
- jsonwebtoken
- multer
- mysql
- path
- sequelize